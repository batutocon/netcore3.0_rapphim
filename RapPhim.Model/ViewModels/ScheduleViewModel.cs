﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class ScheduleCreate
    {
        [Required]
        public int MovieId { get; set; }
        public int Schedule_CateId { get; set; }
        public int RoomId { get; set; }
        public DateTime Date { get; set; }
    }
    public class ScheduleUpdate
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int RoomId { get; set; }
        public int Schedule_CateId { get; set; }
        public DateTime Date { get; set; }
    }
}
