﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class SeatCreate
    {
        [Required]
        public int RoomId { get; set; }
        public string ColumnSeat { get; set; }
        public int Row { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
    }
    public class SeatUpdate
    {
        [Required]
        public int Id { get; set; }
        public int RoomId { get; set; }
        public string ColumnSeat { get; set; }
        public int Row { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
    }
    public class SeatView
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public string ColumnSeat { get; set; }
        public int Row { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int StatusForOrder { get; set; }
    }
}
