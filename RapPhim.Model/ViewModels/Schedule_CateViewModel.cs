﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class Schedule_CateCreate
    {
        [Required]
        public string Time { get; set; }
    }
    public class Schedule_CateUpdate
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Time { get; set; }
    }
}
