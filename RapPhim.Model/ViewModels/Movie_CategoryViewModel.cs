﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class Movie_CategoryCreate
    {
        [Required]
        public int MovieId { get; set; }
        [Required]
        public int CategoryId { get; set; }
    }
}
