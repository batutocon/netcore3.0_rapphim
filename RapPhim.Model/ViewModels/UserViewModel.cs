﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class UserLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; }
    }
    public class UserRegister
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int Sex { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }       
    }
}
