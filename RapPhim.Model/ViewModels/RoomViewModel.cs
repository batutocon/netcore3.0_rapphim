﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class RoomCreate
    {
        [Required]
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
    public class RoomUpdate
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
