﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class Schedule_SeatCreate
    {
        [Required]
        public int ScheduleId { get; set; }
        public int SeatId { get; set; }
    }
    public class Schedule_SeatUpdate
    {
        [Required]
        public int Id { get; set; }
        public int ScheduleId { get; set; }
        public int SeatId { get; set; }
        public string ColumnSeat { get; set; }
        public int Row { get; set; }
        public int Type { get; set; }
    }
}
