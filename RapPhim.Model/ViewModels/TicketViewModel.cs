﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class TicketCreate
    {
        [Required]
        public decimal Price { get; set; }
        public int Schedule_SeatId { get; set; }
    }
    public class TicketUpdate
    {
        [Required]
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int Schedule_SeatId { get; set; }
    }
}
