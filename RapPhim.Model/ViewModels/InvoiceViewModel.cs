﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RapPhim.Model.ViewModels
{
    public class InvoiceCreate
    {
        [Required]
        public string AccountId { get; set; }
        public int AddScore { get; set; }
        public int UseScore { get; set; }
        public int TicketId { get; set; }
    }
    public class InvoiceUpdate
    {
        [Required]
        public int Id { get; set; }
        public string AccountId { get; set; }
        public int AddScore { get; set; }
        public int UseScore { get; set; }
        public int TicketId { get; set; }
    }
}
