﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.ViewModels.Common
{
    public class ApiSuccessResult<T> :ApiResult
    {
        public T result { get; set; }
        public ApiSuccessResult()
        {
            IsSuccessed = true;
        }
        public ApiSuccessResult(T inputResult)
        {
            IsSuccessed = true;
            result = inputResult;
        }
    }
}
