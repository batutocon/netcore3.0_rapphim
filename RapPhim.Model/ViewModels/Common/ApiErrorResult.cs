﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.ViewModels.Common
{
    public class ApiErrorResult<T> :ApiResult
    {
        public T result { get; set; }
        public ApiErrorResult()
        {
            IsSuccessed = false;
        }
        public ApiErrorResult(T inputResult)
        {
            IsSuccessed = false;
            result = inputResult;
        }
    }
}
