﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.ViewModels.Common
{
    public class ApiResult
    {
        public bool IsSuccessed { get; set; }
        public string Message { get; set; }
    }
}
