﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.ViewModels.Common
{
    public class PageResult<T>:ApiResult
    {
        public List<T> lstObj { get; set; }
        public int Count { get; set; }
        public int CountPage { get; set; }
    }
}
