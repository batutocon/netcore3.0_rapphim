﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Director { get; set; }
        public int Duration { get; set; }
        public string Company { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Name_En { get; set; }
        public string UrlImage { get; set; }
    }
}
