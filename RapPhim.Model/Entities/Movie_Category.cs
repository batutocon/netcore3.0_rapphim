﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Movie_Category
    {
        public int MovieId { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public Movie Movie { get; set; }
    }
}
