﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Schedule_Seat
    {
        public int Id { get; set; }
        public int ScheduleId { get; set; }
        public Schedule Schedule { get; set; }
        public int SeatId { get; set; }
        public string ColumnSeat { get; set; }
        public int Row { get; set; }
        public int Type { get; set; }
    }
}
