﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Seat
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public string ColumnSeat { get;set;}
        public int Row { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public Room Room { get; set; }
    }
}
