﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Member
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public string AccountId { get; set; }
        public Account Account { get; set; }
    }
}
