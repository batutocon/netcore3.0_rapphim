﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Schedule
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int Schedule_CateId { get; set; }
        public Schedule_Cate Schedule_Cate { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }
        public DateTime Date { get; set; }
        public Movie Movie { get; set; }
    }
}
