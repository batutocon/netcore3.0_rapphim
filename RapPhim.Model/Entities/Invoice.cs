﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Invoice
    {
        public int Id { get; set; }
        public string AccountId { get; set; }
        public int AddScore { get; set; }
        public int UseScore { get; set; }
        public int TicketId { get; set; }
        public Account Account { get; set; }
        public Ticket Ticket { get; set; }
    }
}
