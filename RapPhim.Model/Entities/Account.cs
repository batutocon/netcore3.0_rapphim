﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Account : IdentityUser<Guid>
    {
        public string FullName { get; set; }
        public DateTime DateofBirth { get; set; }
        public int Sex { get; set; }
        public string IdentityCard { get; set; }
    }
}
