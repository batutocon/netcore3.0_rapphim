﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Model.Entities
{
    public class Ticket
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int Schedule_SeatId { get; set; }
        public  Schedule_Seat Schedule_Seat { get; set; }
    }
}
