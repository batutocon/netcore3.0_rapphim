﻿using RapPhim.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RapPhim.Service.Interfaces;
using RapPhim.Model.Entities;
using System.Globalization;
using RapPhim.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using RapPhim.Model.ViewModels.Common;
using RapPhim.Utilities.Constants;

namespace RapPhim.Api.Controllers
{
    [ApiController]
    [Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN + "," + SystemConstants.Roles.ROLE_NHANVIEN)]
    [Route("api/[controller]")]
    public class SeatController : Controller
    {
        readonly IUnitofWork _unitOfWork;

        public SeatController(IUnitofWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _unitOfWork.GetGenericRepository<Seat>().GetAllRecords();
            return Ok(lst);
        }

        [HttpGet ("{Id}")]
        public ActionResult Index([FromRoute] int Id)
        {
            var obj = _unitOfWork.GetGenericRepository<Seat>().GetById(Id);
            return Ok(obj);
        }

        [HttpPost]
        public ActionResult Create([FromForm] SeatCreate request)
        {
            var obj = new Seat();
            obj.RoomId = request.RoomId;
            obj.ColumnSeat = request.ColumnSeat;
            obj.Row = request.Row;
            obj.Status = request.Status;
            obj.Type = request.Type;
            try
            {
                _unitOfWork.GetGenericRepository<Seat>().Add(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() {IsSuccessed = true, Message = "Create thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Create thất bại" });
            }
        }
        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute]int Id,[FromForm] SeatUpdate request)
        {
            var obj = _unitOfWork.GetGenericRepository<Seat>().GetById(Id);
            if (Id != obj.Id)
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });
            }
            else
            {
                obj.RoomId = request.RoomId;
                obj.Row = request.Row;
                obj.ColumnSeat = request.ColumnSeat;
                obj.Status = request.Status;
                obj.Type = request.Type;
                try
                {
                    _unitOfWork.GetGenericRepository<Seat>().Update(obj);
                    _unitOfWork.SaveChanges();
                    return Ok(new ApiResult() { IsSuccessed = true, Message = "Create thành công" });

                }
                catch
                {
                    return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });

                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _unitOfWork.GetGenericRepository<Seat>().GetById(Id);
            try
            {
                _unitOfWork.GetGenericRepository<Seat>().Remove(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Delete thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Delete thất bại" });
            }
        }
    }
}
