﻿using RapPhim.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RapPhim.Service.Interfaces;
using RapPhim.Model.Entities;
using System.Globalization;
using RapPhim.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using RapPhim.Model.ViewModels.Common;
using RapPhim.Utilities.Constants;

namespace RapPhim.Api.Controllers
{
    [ApiController]
    [Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN + "," + SystemConstants.Roles.ROLE_NHANVIEN)]
    [Route("api/[controller]")]
    public class ScheduleController : Controller
    {
        readonly IUnitofWork _unitOfWork;

        public ScheduleController(IUnitofWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _unitOfWork.GetGenericRepository<Schedule>().GetAllRecords();
            return Ok(lst);
        }

        [HttpGet("{Id}")]
        public ActionResult Index([FromRoute] int Id)
        {
            var obj = _unitOfWork.GetGenericRepository<Schedule>().GetById(Id);
            return Ok(obj);
        }

        [HttpPost]
        public ActionResult Create([FromForm] ScheduleCreate request)
        {
            var obj = new Schedule();
            obj.Date = request.Date;
            obj.MovieId = request.MovieId;
            obj.Schedule_CateId = request.Schedule_CateId;
            obj.RoomId = request.RoomId;
            //check trùng giờ chiếu phim trong cùng 1 phòng khi tạo lịch chiếu
            var phimTrungGio = _unitOfWork.GetGenericRepository<Schedule>().GetFirstOrDefaultByParameter(x => x.Date.Date == obj.Date.Date && x.RoomId == obj.RoomId && x.Schedule_CateId == obj.Schedule_CateId);
            if (phimTrungGio == null)
            {
                try
                {
                    _unitOfWork.GetGenericRepository<Schedule>().Add(obj);
                    _unitOfWork.SaveChanges();
                    return Ok(new ApiResult() { IsSuccessed = true, Message = "Create thành công" });
                }
                catch
                {
                    return Ok(new ApiResult() { IsSuccessed = false, Message = "Create thất bại" });
                }
            }
            else
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Lịch đã trùng" });
            }
        }
        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute] int Id, [FromForm] ScheduleUpdate request)
        {
            var obj = _unitOfWork.GetGenericRepository<Schedule>().GetById(Id);
            if (Id != obj.Id)
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });
            }
            else
            {
                obj.Date = request.Date;
                obj.MovieId = request.MovieId;
                obj.Schedule_CateId = request.Schedule_CateId;
                obj.RoomId = request.RoomId;
                //check trùng giờ chiếu phim trong cùng 1 phòng khi tạo lịch chiếu
                var phimTrungGio = _unitOfWork.GetGenericRepository<Schedule>().GetFirstOrDefaultByParameter(x => x.Date.Date == obj.Date.Date && x.RoomId == obj.RoomId && x.Schedule_CateId == obj.Schedule_CateId);
                if (phimTrungGio != null)
                {
                    try
                    {
                        _unitOfWork.GetGenericRepository<Schedule>().Add(obj);
                        _unitOfWork.SaveChanges();
                        return Ok(new ApiResult() { IsSuccessed = true, Message = "Update thành công" });
                    }
                    catch
                    {
                        return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });
                    }
                }
                else
                {
                    return Ok(new ApiResult() { IsSuccessed = false, Message = "Lịch đã trùng" });
                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _unitOfWork.GetGenericRepository<Schedule>().GetById(Id);
            try
            {
                _unitOfWork.GetGenericRepository<Schedule>().Remove(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Delete thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Delete thất bại" });
            }
        }
    }
}
