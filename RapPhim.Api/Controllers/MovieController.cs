﻿using RapPhim.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RapPhim.Service.Interfaces;
using RapPhim.Model.Entities;
using System.Globalization;
using RapPhim.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using RapPhim.Model.ViewModels.Common;
using RapPhim.Utilities.Constants;

namespace RapPhim.Api.Controllers
{
    [ApiController]
    [Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN + "," + SystemConstants.Roles.ROLE_NHANVIEN)]
    [Route("api/[controller]")]
    public class MovieController : Controller
    {
        readonly IUnitofWork _unitOfWork;

        public MovieController(IUnitofWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _unitOfWork.GetGenericRepository<Movie>().GetAllRecords();
            return Ok(lst);
        }

        [HttpGet("{pagesize}/{page}")]
        public async Task<ActionResult<PageResult<Movie>>> Filter(string key = "", [FromRoute] int pagesize = 3, [FromRoute] int page = 1)
        {
            var lst = await _unitOfWork.GetGenericRepository<Movie>().GetRecordsToShow(page, pagesize, x => x.Name.Contains(key));
            return lst;
        }

        [HttpGet("{Id}")]
        public ActionResult Index([FromRoute] int Id)
        {
            var obj = _unitOfWork.GetGenericRepository<Movie>().GetById(Id);
            return Ok(obj);
        }

        //[Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN)]
        [HttpPost]
        public ActionResult Create([FromForm] MovieCreate request)
        {
            var obj = new Movie();
            obj.Name = request.Name;
            obj.Company = request.Company;
            obj.Content = request.Content;
            obj.Director = request.Director;
            obj.Duration = obj.Duration;
            obj.EndDate = obj.EndDate;
            obj.Name_En = obj.Name_En;
            obj.StartDate = obj.StartDate;
            obj.UrlImage = obj.UrlImage;
            try
            {
                _unitOfWork.GetGenericRepository<Movie>().Add(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Create thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Create thất bại" });
            }
        }

        //[Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN)]
        [HttpPost("CreateCategory")]
        public ActionResult CreateCategory([FromForm] Movie_CategoryCreate request)
        {
            var obj = new Movie_Category();
            obj.CategoryId = request.CategoryId;
            obj.MovieId = request.MovieId;
            try
            {
                _unitOfWork.GetGenericRepository<Movie_Category>().Add(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Create thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Create thất bại" });
            }
        }

        //[Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN)]
        [HttpDelete("DeleteCategory")]
        public ActionResult DeleteCategory(int phimId, int cateId)
        {
            var obj = _unitOfWork.GetGenericRepository<Movie_Category>().GetFirstOrDefaultByParameter(x => x.MovieId == phimId && x.CategoryId == cateId);
            try
            {
                _unitOfWork.GetGenericRepository<Movie_Category>().Remove(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Delete thành công" });
            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Delete thất bại" });
            }
        }

        //[Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN)]
        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute] int Id, [FromForm] MovieUpdate request)
        {
            var obj = _unitOfWork.GetGenericRepository<Movie>().GetById(Id);
            if (Id != obj.Id)
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });
            }
            else
            {
                obj.Name = request.Name;
                obj.Company = request.Company;
                obj.Content = request.Content;
                obj.Director = request.Director;
                obj.Duration = obj.Duration;
                obj.EndDate = obj.EndDate;
                obj.Name_En = obj.Name_En;
                obj.StartDate = obj.StartDate;
                obj.UrlImage = obj.UrlImage;
                try
                {
                    _unitOfWork.GetGenericRepository<Movie>().Update(obj);
                    _unitOfWork.SaveChanges();
                    return Ok(new ApiResult() { IsSuccessed = true, Message = "Create thành công" });

                }
                catch
                {
                    return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });

                }
            }
        }

        //[Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN)]
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _unitOfWork.GetGenericRepository<Movie>().GetById(Id);
            try
            {
                _unitOfWork.GetGenericRepository<Movie>().Remove(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Delete thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Delete thất bại" });
            }
        }
    }
}
