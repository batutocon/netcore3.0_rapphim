﻿using RapPhim.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RapPhim.Service.Interfaces;
using RapPhim.Model.Entities;
using System.Globalization;
using RapPhim.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using RapPhim.Model.ViewModels.Common;
using RapPhim.Utilities.Constants;

namespace RapPhim.Api.Controllers
{
    [ApiController]
    [Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN + "," + SystemConstants.Roles.ROLE_NHANVIEN)]
    [Route("api/[controller]")]
    public class Schedule_CateController : Controller
    {
        readonly IUnitofWork _unitOfWork;

        public Schedule_CateController(IUnitofWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _unitOfWork.GetGenericRepository<Schedule_Cate>().GetAllRecords();
            return Ok(lst);
        }

        //[HttpGet("{pagesize}/{page}")]
        //public async Task<ActionResult<PageResult<Schedule_Cate>>> Filter(string key = "", [FromRoute] int pagesize = 3, [FromRoute] int page = 1)
        //{
        //    var lst = await _unitOfWork.GetGenericRepository<Schedule_Cate>().GetRecordsToShow(page, pagesize, x => x.Name.Contains(key));
        //    return lst;
        //}


        [HttpGet ("{Id}")]
        public ActionResult Index([FromRoute] int Id)
        {
            var obj = _unitOfWork.GetGenericRepository<Schedule_Cate>().GetById(Id);
            return Ok(obj);
        }

        [HttpPost]
        public ActionResult Create([FromForm] Schedule_CateCreate request)
        {
            var obj = new Schedule_Cate();
            obj.Time = request.Time;            
            try
            {
                _unitOfWork.GetGenericRepository<Schedule_Cate>().Add(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() {IsSuccessed = true, Message = "Create thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Create thất bại" });
            }
        }
        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute]int Id,[FromForm] Schedule_CateUpdate request)
        {
            var obj = _unitOfWork.GetGenericRepository<Schedule_Cate>().GetById(Id);
            if (Id != obj.Id)
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });
            }
            else
            {
                obj.Time = request.Time;
                try
                {
                    _unitOfWork.GetGenericRepository<Schedule_Cate>().Update(obj);
                    _unitOfWork.SaveChanges();
                    return Ok(new ApiResult() { IsSuccessed = true, Message = "Create thành công" });

                }
                catch
                {
                    return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });

                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _unitOfWork.GetGenericRepository<Schedule_Cate>().GetById(Id);
            try
            {
                _unitOfWork.GetGenericRepository<Schedule_Cate>().Remove(obj);
                _unitOfWork.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Delete thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Delete thất bại" });
            }
        }
    }
}
