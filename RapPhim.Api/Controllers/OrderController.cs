﻿using Microsoft.AspNetCore.Mvc;
using RapPhim.Model.Entities;
using RapPhim.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RapPhim.Model.ViewModels.Common;
using RapPhim.Model.ViewModels;
using RapPhim.Utilities.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace RapPhim.Api.Controllers
{
    [ApiController]
    //[Authorize]
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        readonly IUnitofWork _ouw;
        private readonly UserManager<Account> _userManager;

        public OrderController(IUnitofWork ouw, UserManager<Account> userManager)
        {
            _ouw = ouw;
            _userManager = userManager;
        }
        [HttpGet("listmovie/{date}")]
        public IActionResult ListPhim([FromRoute] DateTime date)
        {
            IEnumerable<Schedule> lstLichChieuHomNay = _ouw.GetGenericRepository<Schedule>().Querry().Where(x=>x.Date.Date == date.Date);
            if (lstLichChieuHomNay.ToList().Count==0)
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Chưa có lịch chiếu" });
            }
            List<Movie> phimHomNay = _ouw.GetGenericRepository<Movie>().Querry().Join(lstLichChieuHomNay, movie => movie.Id, schedule => schedule.MovieId, (movie, schedule) => new Movie()
            {
                Company = movie.Company,
                Content = movie.Content,
                Director = movie.Director,
                Duration = movie.Duration,
                EndDate = movie.EndDate,
                Id = movie.Id,
                Name = movie.Name,
                Name_En = movie.Name_En,
                StartDate = movie.StartDate,
                UrlImage = movie.UrlImage
            }).ToList() ;

            return Ok(phimHomNay);
        }

        [HttpGet("listschedule/{movieId}/{date}")]
        public IActionResult ListSchedule([FromRoute] int movieId, [FromRoute] DateTime date)
        {
            List<Schedule> lstLichChieuHomNay = _ouw.GetGenericRepository<Schedule>().Querry().Where(x => x.Date.Date == date.Date && x.MovieId == movieId).Include(t=>t.Schedule_Cate).Include(t=> t.Room ).Include(t => t.Movie).ToList();          
            return Ok(lstLichChieuHomNay);
        }

        [HttpGet("listseat/{movieId}/{scheduleId}")]
        public IActionResult ListSeat([FromRoute] int movieId, [FromRoute] int scheduleId)
        {
            Schedule schedule = _ouw.GetGenericRepository<Schedule>().GetById(scheduleId);
            if (schedule==null)
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message ="Lịch chiếu không tồn tại"});
            }
            List<Seat> lstSeat = _ouw.GetGenericRepository<Seat>().GetListByParameter(x => x.RoomId == schedule.RoomId).ToList();
            List<Schedule_Seat> lstSeatOrdered = _ouw.GetGenericRepository<Schedule_Seat>().GetListByParameter(x => x.ScheduleId == scheduleId).ToList();
            List<SeatView> lstrs = new List<SeatView>();
            foreach (var seat in lstSeat)
            {
                var SeatView = new SeatView() {
                    ColumnSeat = seat.ColumnSeat,
                    Id = seat.Id,
                    RoomId = seat.RoomId,
                    Row = seat.Row,
                    Status = seat.Status,
                    StatusForOrder = SystemConstants.Seat.CHUAORDER,
                    Type = seat.Type
                };
                lstrs.Add(SeatView);
            }
            foreach (var seat in lstrs)
            {
                if (lstSeatOrdered.FirstOrDefault(x=>x.Id == seat.Id) != null)
                {
                    seat.StatusForOrder = SystemConstants.Seat.DAORDER;
                }
            }
            return Ok(lstrs);
        }

        [HttpPost]
        public ActionResult CreateOrder([FromForm] Schedule_SeatCreate request)
        {
            var obj = new Schedule_Seat();
            obj.ScheduleId = request.ScheduleId;
            obj.SeatId = request.SeatId;
            var checkGheTrong = _ouw.GetGenericRepository<Schedule_Seat>().GetFirstOrDefaultByParameter(x => x.ScheduleId == obj.ScheduleId && x.SeatId == obj.SeatId);
            if (checkGheTrong!=null)
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Ghế đã được đặt" });
            }

            var seat = _ouw.GetGenericRepository<Seat>().GetById(obj.SeatId);
            obj.ColumnSeat = seat.ColumnSeat;
            obj.Row = seat.Row;
            obj.Type = seat.Type;
            try
            {
                //thêm đặt chỗ
                _ouw.GetGenericRepository<Schedule_Seat>().Add(obj);
                _ouw.SaveChanges();

                //thêm vé
                Ticket ticket = new Ticket();
                ticket.Schedule_SeatId = obj.Id;
                ticket.Price = SystemConstants.Seat.PRICE_VIP;
                _ouw.GetGenericRepository<Ticket>().Add(ticket);
                _ouw.SaveChanges();

                //thêm hóa đơn
                var username = User.Identity.Name;
                Invoice inv = new Invoice() { 
                    AccountId = username,
                    AddScore = 0,
                    TicketId = ticket.Id,
                    UseScore = 0,
                };
                _ouw.GetGenericRepository<Invoice>().Add(inv);
                _ouw.SaveChanges();
                var rs = Newtonsoft.Json.JsonConvert.SerializeObject(inv);
                return Ok(new ApiResult() { IsSuccessed = true, Message = rs });
            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Create thất bại" });
            }
        }
    }
}
