﻿
using RapPhim.Model.ViewModels;
using RapPhim.Model.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapPhim.ClientApi.Interfaces
{
    public interface IUserApiClient
    {
        Task<string> Authenticate(UserLogin request);
        Task<ApiResult> RegisterUser(UserRegister registerRequest);
    }
}