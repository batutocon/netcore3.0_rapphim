﻿
using RapPhim.Model.Entities;
using RapPhim.Model.ViewModels;
using RapPhim.Model.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapPhim.ClientApi.Interfaces
{
    public interface IOrderApiClient
    {
        Task<List<Movie>> ListPhim(DateTime date);
        Task<List<Schedule>> ListLichChieu(int movieId, DateTime date);
        Task<List<SeatView>> ListSeat(int movieId, int scheduleId);
    }
}