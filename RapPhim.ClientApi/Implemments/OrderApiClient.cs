﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RapPhim.ClientApi.Interfaces;
using RapPhim.Model.Entities;
using RapPhim.Model.ViewModels;
using RapPhim.Model.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RapPhim.ClientApi.Implemments
{
    public class OrderApiClient : IOrderApiClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public OrderApiClient(IHttpClientFactory httpClientFactory,
                   IHttpContextAccessor httpContextAccessor,
                    IConfiguration configuration)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<List<Schedule>> ListLichChieu(int movieId, DateTime date)
        {
            var lstrs = new List<Schedule>();
            var dateParam = date.ToString("yyyy-MM-dd");
            var sessions = _httpContextAccessor
                           .HttpContext
                           .Session
                           .GetString("Token");

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
            try
            {
                var response = await client.GetAsync("api/Order/ListSchedule/" + movieId+"/" +dateParam);
                var body = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    //var myDeserializedObjList = JsonConvert.DeserializeObject(body);
                    lstrs = JsonConvert.DeserializeObject<List<Schedule>>(body);
                    return lstrs;
                }
            }
            catch (Exception ex)
            {
                return lstrs;
            }

            return lstrs;
        }

        public async Task<List<Movie>> ListPhim(DateTime date)
        {
            var lstrs = new List<Movie>();
            var dateParam = date.ToString("yyyy-MM-dd");
            var sessions = _httpContextAccessor
                           .HttpContext
                           .Session
                           .GetString("Token");

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
            try
            {
                var response = await client.GetAsync("api/Order/listmovie/" + dateParam);
                var body = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    //var myDeserializedObjList = JsonConvert.DeserializeObject(body);
                    lstrs = JsonConvert.DeserializeObject<List<Movie>>(body);
                    return lstrs;
                }
            }
            catch(Exception ex)
            {
                return lstrs;
            }

            return lstrs;
        }

        public async Task<List<SeatView>> ListSeat(int movieId, int scheduleId)
        {
            var lstrs = new List<SeatView>();
            var sessions = _httpContextAccessor
                           .HttpContext
                           .Session
                           .GetString("Token");

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
            try
            {
                var url = "api/Order/ListSeat/" + movieId + "/" + scheduleId;
                var response = await client.GetAsync(url);
                var body = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    //var myDeserializedObjList = JsonConvert.DeserializeObject(body);
                    lstrs = JsonConvert.DeserializeObject<List<SeatView>>(body);
                    return lstrs;
                }
            }
            catch (Exception ex)
            {
                return lstrs;
            }

            return lstrs;
        }
    }
}
