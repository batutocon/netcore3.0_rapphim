﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Utilities.Constants
{
    public class SystemConstants
    {    
        public class Roles
        {
            public const string ROLE_ADMIN = "admin";
            public const string ROLE_USER = "user";
            public const string ROLE_NHANVIEN = "nhanvien";
        }
        public class Seat
        {
            // giá ghế
            public const decimal PRICE_VIP = 100000;
            public const decimal PRICE_NORMAL = 50000;

            //trạng thái ghế
            public const int BAOTRI = 0;
            public const int HOATDONG = 1;

            //trạng thái ghế đang order
            public const int DAORDER = 0;
            public const int CHUAORDER = 1;
        }
    }
}