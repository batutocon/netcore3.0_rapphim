﻿using RapPhim.Service.GenericRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Service.Interfaces
{
    public interface IUnitofWork
    {
        GenericRepository<T> GetGenericRepository<T>() where T : class;
        void SaveChanges();
    }
}
