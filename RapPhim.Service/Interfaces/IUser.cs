﻿using RapPhim.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RapPhim.Service.Interfaces
{
    public interface IUser
    {
        Task<string> Authen(UserLogin rq);
        Task<bool> Register(UserRegister rq);
    }
}
