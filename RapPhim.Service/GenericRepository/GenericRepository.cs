﻿using Microsoft.EntityFrameworkCore;
using RapPhim.Model;
using RapPhim.Model.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RapPhim.Service.GenericRepository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private RapphimContext _context;
        DbSet<T> _dbSet;
        public GenericRepository(RapphimContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }
        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public IQueryable<T> Querry()
        {
            return _context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public IEnumerable<T> GetAllRecords()
        {
            return _dbSet.ToList();
        }

        public IQueryable<T> GetAllRecordsIQueryable()
        {
            return _dbSet;
        }

        public async Task<PageResult<T>> GetRecordsToShow(int pageNo, int pageSize, Expression<Func<T, bool>> wherePredict)
        {
            var lstobj = _dbSet.Where(wherePredict);

            int totalRow = await lstobj.CountAsync();
            var lstrs = lstobj.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            int countPage = totalRow / lstrs.Count() +1;
            var ObjReturn = new PageResult<T>()
            {
                lstObj = lstrs,
                Count = totalRow,
                CountPage = countPage,
                IsSuccessed = true,
                Message = "",
            };
            return ObjReturn;
        }

        public int GetAllRecordsCount()
        {
            return _dbSet.Count();
        }

        public void Add(T entity)
        {
            try
            {
                _dbSet.Add(entity);
                //_context.SaveChanges();
            }
            catch (Exception e)
            {
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    foreach (var ve in eve.ValidationErrors)
                //    {

                //    }
                //}
                //throw;
            }

        }

        /// <summary>
        /// Updates table entity passed to it
        /// </summary>
        /// <param name="entity"></param>
        public void Update(T entity)
        {
            _dbSet.Attach(entity);
            if (_context.Entry(entity).State == EntityState.Modified)
            {
                _context.Entry(entity).State = EntityState.Added;
                _context.Entry(entity).State = EntityState.Modified;
            }
            _context.Entry(entity).State = EntityState.Modified;
        }
        public void DeAttach(T entity)
        {
            _context.Entry(entity).State = EntityState.Detached;
        }
        public void UpdateByWhereClause(Expression<Func<T, bool>> wherePredict, Action<T> forEachPredict)
        {
            _dbSet.Where(wherePredict).ToList().ForEach(forEachPredict);
        }

        public T GetFirstOrDefault(int recordId)
        {
            return _dbSet.Find(recordId);
        }

        public T GetFirstOrDefaultByParameter(Expression<Func<T, bool>> wherePredict)
        {
            return _dbSet.Where(wherePredict).FirstOrDefault();
        }

        public IEnumerable<T> GetListByParameter(Expression<Func<T, bool>> wherePredict)
        {
            return _dbSet.Where(wherePredict).ToList();
        }

        public void Remove(T entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
                _dbSet.Attach(entity);
            _dbSet.Remove(entity);
        }

        public void RemoveByWhereClause(Expression<Func<T, bool>> wherePredict)
        {
            T entity = _dbSet.Where(wherePredict).FirstOrDefault();
            Remove(entity);
        }

        public void RemoveRangeByWhereClause(Expression<Func<T, bool>> wherePredict)
        {
            List<T> entity = _dbSet.Where(wherePredict).ToList();
            foreach (var ent in entity)
            {
                Remove(ent);
            }
        }
        public void DeleteMarkByWhereClause(Expression<Func<T, bool>> wherePredict, Action<T> ForEachPredict)
        {
            _dbSet.Where(wherePredict).ToList().ForEach(ForEachPredict);
            _context.SaveChanges();
        }

        public void InactiveAndDeleteMarkByWhereClause(Expression<Func<T, bool>> wherePredict, Action<T> ForEachPredict)
        {
            _dbSet.Where(wherePredict).ToList().ForEach(ForEachPredict);
            _context.SaveChanges();
        }

        /// <summary>
        /// Returns result by where clause in descending order
        /// </summary>
        /// <param name="orderByPredict"></param>
        /// <returns></returns>
        public IQueryable<T> OrderByDescending(Expression<Func<T, int>> orderByPredict)
        {
            if (orderByPredict == null)
            {
                return _dbSet;
            }
            return _dbSet.OrderByDescending(orderByPredict);
        }

        public IEnumerable<T> GetResultBySqlProcedure(string query, params object[] parameters)
        {
            if (parameters != null)
            {
                IEnumerable<T> rs = (IEnumerable<T>)_context.Set<T>().FromSqlRaw(query, parameters).ToList();
                return rs;
            }
            // return _context.BookingOfficesDaCoTrip.FromSqlRaw(query, parameters).ToList();
            else
            {
                IEnumerable<T> rs = (IEnumerable<T>)_context.Set<T>().FromSqlRaw(query, parameters).ToList();
                return rs;
            }
        }
        /// <summary>
        /// Executes procedure in database and returns result
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
    }
}
