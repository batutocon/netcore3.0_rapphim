﻿using RapPhim.Model;
using RapPhim.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RapPhim.Service.GenericRepository
{
    public class GenericUnitOfWork : IUnitofWork
    {
        protected readonly RapphimContext DBEntity;

        public GenericUnitOfWork(RapphimContext context)
        {
            DBEntity = context;
        }

        public GenericRepository<T> GetGenericRepository<T>() where T : class
        {
            return new GenericRepository<T>(DBEntity);
        }

        public void SaveChanges()
        {
            DBEntity.SaveChanges();
        }


        #region Disposing the Unit of work context ...
        private bool disposed = false;


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    DBEntity.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

      

        #endregion
    }

}
