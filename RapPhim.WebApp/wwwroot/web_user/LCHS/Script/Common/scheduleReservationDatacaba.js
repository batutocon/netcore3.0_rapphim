﻿
/************************************************************************************************* 
 *  @Author : 이인수 (is@madive.co.kr)
 *  @Description : web 상영스케쥴
 *  @Version  1.0
************************************************************************************************/

var scheduleUtill;
var schedulePlayUtill;


var BLOCKSIZE = 3;
var PAGENO = 1;
var BLOCKSIZE2 = 3;
var PAGENO2 = 1;
var list_view1_cnt = 1;
var list_view2_cnt = 1;
var tabIndex = 0;   // 2018-08-20 처음 페이지 로딩 시 Tab에 대한 Click trigger 작동 제어용-이상균

$(function () {

    //설정위치 이동 $('.ad_pic').hide();

    $.fn.cookieChanges('del', 'ticketingState.schedule.cinemaId', '');
    $.fn.cookieChanges('del', 'ticketingState.ticketing.playDate', '');
    $.fn.cookieChanges('del', 'ticketingState.schedule.movieArea', '');
    $.fn.cookieChanges('del', 'ticketingState.schedule.movieId', '');
    $.fn.cookieChanges('del', 'ticketingState.schedule.movieSort', '');

    $.fn.scheduleReservation = function (option) {
        var S = $.extend({ target: '.cont_ticket', leng: 'LL' }, option);

        this.step = '';

        this.target = $(S.target);
        this.calendar = this.target.find('.calendar');
        this.monthArray = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        this.date = '';
        this.week = '';
        this.dateTotal = 0;
        this.dateCurNo = 0;

        this.tabResevation = this.target.find('.tab_st08');

        this.area = $('.cinema_twrap');
        this.areaList = this.area.find('.theater_zone');

        this.favorite = this.area.find('.ticket_My');

        this.cookieDate = '';
        this.cinemaID = '';
        this.movieArea = '';
        this.movieID = '';
        this.movieSort = '';
        this.playSequenceCode = '';

        this.movie = $('.movie_twrap');
        this.movieList = this.movie.find('.m_List');
        this.movieSum = 0;
        this.movieNo = 0;

        this.cineWrap = $('.listViewCinema');
        this.cineViewList = this.cineWrap.find('.time_line');

        this.movieWrap = $('.listViewMovie');
        this.movieViewList = this.movieWrap.find('.time_line');
        this.movieWrapArea = this.movieWrap.find('.tab_st09');
        
        this.leng = S.leng;
        
        // 회원 정보 ////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (memberInfo.MemberNoOn == 'undefined' || memberInfo.MemberNoOn == '' || memberInfo.MemberNoOn == null) {
            this.login = false;
            this.memberID = '';
        } else {
            this.login = true;
            this.memberID = memberInfo.MemberNoOn;
        };

        this.init();
    };

    $.fn.scheduleReservation.prototype = {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 스케쥴 데이터 호출
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        init: function () {
            
            var obj = { MethodName: "GetTicketingPage", channelType: "HO", osType: BrowserAgent(), osVersion: navigator.userAgent, memberOnNo: this.memberID };
            JsonCallSync(CinemaServiceDomain + '/LCWS/Ticketing/TicketingData.aspx', obj, this.jsonData);
            
            this.stepCheck();
        },
        jsonData: function (data) {
            try {
                scheduleUtill = data.responseJSON;
            }
            catch (e) {
                //alert('error');
            }
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 쿠키 체크
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stepCheck: function () {
            this.step = cookieJson.ticketingState.schedule.tabIdx;

            if (scheduleUtill != 'undefinded') {
                if(this.step == 0 || this.step == '') { // 진입 체크 '0':영화관별, '1':영화별
                    this.movie.hide();
                    this.movieWrap.hide();
                    this.area.show();
                    this.cineWrap.show();
                } else {
                    this.movie.show();
                    this.movieWrap.show();
                    this.area.hide();
                    this.cineWrap.hide();
                };

                //this.tabResevation.find('a').eq(this.step).addClass('on').parent().siblings().find('a').removeClass('on');
                this.tabResevation.find('a').eq(this.step).addClass('on').parent().parent().siblings().find('a').removeClass('on');


                this.dataDraw();
                this.cinemaDraw();
            };
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 데이터 드로우 - 달력 리스트, 영화관 리스트, 선호영화관 리스트, 영화 리스트
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dataDraw: function () {
            var T = this;
            var html = '';

            var dateNode = $(scheduleUtill.MoviePlayDates.Items.Items); //달력 노드
            var YearData = dateNode[0].Year; // 시작 년도
            var monthData = dateNode[0].Month; // 시작달

            T.cookieDate = cookieJson.ticketingState.schedule.playDateId;
            T.cinemaID = cookieJson.ticketingState.schedule.cinemaId;
            T.movieArea = cookieJson.ticketingState.schedule.movieArea;
            T.movieSort = cookieJson.ticketingState.schedule.movieSort;
            T.playSequenceCode = cookieJson.ticketingState.schedule.playSequenceId;
            
            T.calendar.find('fieldset').append('<span class="month" style="top:-47px; left:47px;"><em>' + monthData + '</em><span>' + YearData + ' ' + T.monthArray[monthData-1] + '</span></span>');

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // 달력 드로우
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            T.dateTotal = Math.ceil(dateNode.length / 7) - 1; // 2018-09-28 이상균 : 14일 일자 표기를 7일로 변경 T.dateTotal = Math.ceil(dateNode.length / 14) - 1;
            var dateInterval = 0;

            dateNode.each(function (n) {
                if (T.leng == 'EN') {
                    var weekData = $(this)[0].DayOfWeekEN; // 요일 string
                } else {
                    var weekData = $(this)[0].DayOfWeek; // 요일 string
                }
                var YearData2 = $(this)[0].Year; // 시작 년도
                var monthData2 = $(this)[0].Month; // 시작달
                var dayData = $(this)[0].Day; // 일 string
                var playData = $(this)[0].PlayDate; // 일 string
                var activeDate = $(this)[0].IsPlayDate;

                if (T.cookieDate == playData) T.dateCookie = n;

                html = '<input type="radio" name="day" value="' + playData + '" id="' + T.monthArray[monthData2 - 1] + dayData + '">';
                html += '<label for="' + T.monthArray[monthData2 - 1] + dayData + '" class="month-picker-label" style="left:' + (n * 120) + 'px"><span>' + weekData + '</span><em>' + dayData + '</em></label>'; // 2018-09-28 이상균 : 14일 일자 표기를 7일로 변경 ( n*60->n*120 )

                T.calendar.find('.calendarArea').append(html).find('label').eq(n).data({ 'date': playData, 'year': YearData2, 'month': monthData2, 'day': dayData, 'week': weekData, 'active': activeDate });

                if (activeDate == 'N') {
                    T.calendar.find('.calendarArea').find('input').eq(n).attr('disabled', true);
                    T.calendar.find('.calendarArea').find('label').eq(n).addClass('noDate');
                };

                switch ($(this)[0].DayOfWeekEN) { //주말 체크  // 2018-07-20 영문명을 가지고 체크하는 방법으로 수정
                    case 'SAT':
                        T.calendar.find('.calendarArea').find('label').eq(n).addClass('sat');
                        break;
                    case 'SUN':
                        T.calendar.find('.calendarArea').find('label').eq(n).addClass('sun');
                        break;
                };

                // 2016.04.06 추가
                if ($(this)[0].HolidayYN == 'Y'
                    && !T.calendar.find('.calendarArea').find('label').eq(n).hasClass('sat')
                    && !T.calendar.find('.calendarArea').find('label').eq(n).hasClass('sun')) {
                    T.calendar.find('.calendarArea').find('label').eq(n).addClass('sun');
                };

                if (monthData != monthData2) { // 년도 및 월 체인지 체크
                    monthData = monthData2;
                    YearData = YearData2;
                    var pos = T.calendar.find('.calendarArea').find('label').last().position();
                    T.calendar.find('fieldset').append('<span class="month" style="top:-47px; left:' + ((pos.left) + 47) + 'px"><em>' + monthData2 + '</em><span>' + YearData2 + ' ' + T.monthArray[monthData2 - 1] + '</span></span>');

                    if (activeDate == 'N') T.calendar.find('fieldset').find('> span').last().addClass('noDate');

                    if (n > 0 && n < 3) {
                        T.calendar.find('fieldset').find('> span').eq(0).hide();
                    };

                    if (n >= 4 && n <= 6) { // 2018-09-28 이상균 : 14일 일자 표기를 7일로 변경 if (n >= 11 && n <= 13) {
                        T.calendar.find('fieldset').find('> span').last().find('span').hide();
                    };

                    if (n > 6) { // 2018-09-28 이상균 : 14일 일자 표기를 7일로 변경 if (n > 13) {
                        T.calendar.find('fieldset').find('> span').last().hide();
                    };
                };
            });

            if (T.cookieDate != '') {
                T.calendar.find('.calendarArea').find('input').eq(T.dateCookie).prop('checked', true);
                T.calendar.find('.calendarArea').find('label').eq(T.dateCookie).data('date');
                T.date = T.calendar.find('.calendarArea').find('input').eq(T.dateCookie).val();

                T.dateCurNo = Math.ceil((T.dateCookie + 1) / 7) - 1; // 2018-09-28 이상균 : 14일 일자 표기를 7일로 변경 T.dateCurNo = Math.ceil((T.dateCookie + 1) / 14) - 1;
                if (T.dateCurNo == -1) T.dateCurNo = 0;
                T.calendar.find('.calendarArea').find('label').css({ 'left': '-=' + (840 * T.dateCurNo) });
                T.calendar.find('fieldset').find('> span').css({ 'left': '-=' + (840 * T.dateCurNo) });
            } else {
                T.calendar.find('.calendarArea').find('input').first().prop('checked', true);
                T.calendar.find('.calendarArea').find('label').first().data('date');
                T.date = T.calendar.find('.calendarArea').find('input').first().val();
            };

            if (dateNode.length < 8) { // 2018-09-28 이상균 : 14일 일자 표기를 7일로 변경 if (dateNode.length < 15) {
                T.calendar.find('> a').addClass('nodata');
            } else {
                if (T.dateCurNo == 0) {
                    T.calendar.find('.prev').addClass('nodata').siblings('a').removeClass('nodata');
                } else if (T.dateCurNo == T.dateTotal) {
                    T.calendar.find('.next').addClass('nodata').siblings('a').removeClass('nodata');
                } else if (T.dateCurNo > 0 && T.dateCurNo < T.dateTotal) {
                    T.calendar.find('> a').removeClass('nodata');
                };
            };

            $.fn.cookieChanges('add', 'ticketingState.ticketing.playDate', T.date);
            $.fn.cookieChanges('add', 'ticketingState.ticketing.playWeek', T.calendar.find('.calendarArea').find('label').first().data('week'));
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 영화관 드로우
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        cinemaDraw: function () {
            var T = this;
            var html = '';

            if (T.login && scheduleUtill.FavoriteCinemas.Cinemas != null) {
                var favoriteNode = $(scheduleUtill.FavoriteCinemas.Cinemas.Items); //선호영화관 노드
            };
            var areaNode = $(scheduleUtill.CinemaDivison.AreaDivisions.Items); //지역 노드
            var cinemaNode = $(scheduleUtill.Cinemas.Cinemas.Items); //영화관 노드

            areaNode.sort(function (a, b) { // 지역 정렬
                return a.SortSequence < b.SortSequence ? -1 : a.SortSequence > b.SortSequence ? 1 : 0;
            });
            cinemaNode.sort(function (a, b) { // 영화관 정렬
                return a.SortSequence < b.SortSequence ? -1 : a.SortSequence > b.SortSequence ? 1 : 0;
            });

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // 지역 드로우
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            areaNode.each(function (n) {
                var areaData = $(this)[0].DivisionCode;
                var typeData = Number($(this)[0].DetailDivisionCode);
                var noData = $(this)[0].SortSequence;
                var countData = $(this)[0].CinemaCount;

                if (T.leng == 'EN') {
                    var nameData = $(this)[0].GroupNameUS;
                } else {
                    var nameData = $(this)[0].GroupName;
                }

                html = '<li>';
                // 2016.05.03 웹접근성
                html += '   <span class="area_zone zone_0' + (n + 1) + '"><a href="javascript:void(0);" class="area_btn">' + nameData + '(' + countData + ')</a></span>';
                html += '   <div class="area_cont">';
                html += '       <h3 class="blind">' + nameData + '</h3>';
                html += '       <ul class="area_list d' + typeData + '">';
                html += '       </ul>';
                html += '   </div>';
                html += '</li>';

                T.areaList.append(html).find('.d' + typeData).data({ 'area': typeData });

                html = '<li>';
                html += '   <a href="javascript:void(0);">' + nameData + '(' + countData + ')</a>';
                html += '</li>';

                T.movieWrapArea.append(html).find('a').last().data({ 'area': '1|' + typeData + '|0000' });
            });

            if (T.movieArea == '' || T.movieArea == null || T.movieArea == 'undefined') {
                $.fn.cookieChanges('add', 'ticketingState.schedule.movieArea', '0');
                T.movieWrapArea.find('a').first().addClass('on');

                // 2016.05.03 웹접근성
                $('#title_h4').text(T.movieWrapArea.find('a').first().text());
                $('#title_h4_2').text(T.movieWrapArea.find('a').first().text());
            } else {
                T.movieWrapArea.find('a').eq(Number(T.movieArea)).addClass('on');

                // 2016.05.03 웹접근성
                $('#title_h4').text(T.movieWrapArea.find('a').eq(Number(T.movieArea)).text());
                $('#title_h4_2').text(T.movieWrapArea.find('a').eq(Number(T.movieArea)).text());
            };

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // 영화관 드로우
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            cinemaNode.each(function (n) {
                var typeData = $(this)[0].DivisionCode;
                var areaData = Number($(this)[0].DetailDivisionCode);
                var codeData = $(this)[0].CinemaID;
                var noData = $(this)[0].SortSequence;
                var spVar = 0;

                if (T.leng == 'EN') {
                    var nameData = $(this)[0].CinemaNameUS;
                } else {
                    var nameData = $(this)[0].CinemaName;
                }

                html = '<li>';
                html += '   <a href="javascript:void(0);">' + nameData + '</a>';
                html += '</li>';

                if (typeData != 100) { // 데이터 분기 '100' 은 일반 영화관
                    T.area.find('.d' + areaData).append(html).find('a').last().addClass('1' + areaData + '' + codeData).data({ 'cinemaClass': '1' + areaData + '' + codeData, 'cinemaID': '1|' + areaData + '|' + codeData });
                };
            });

            T.areaList.find('.area_zone').find('a').removeClass('on').parent().next().hide().removeClass('on');

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // 쿠키 드로우
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (T.cinemaID != '') {
                var insID = T.cinemaID.replace(/\|/g, '');
                T.areaList.find('.' + insID).addClass('on').parents('.area_cont').show().addClass('on').prev().find('a').addClass('on');
            } else {
                T.areaList.find('.area_zone').eq(0).find('a').addClass('on').parent().next().show().addClass('on');
            };

            T.movieDraw();

            if ($('.area_cont.on').find('.area_list').find('a.on').length > 0) {
                // 2016.05.10 웹접근성
                $('#title_h5').text($('.area_cont.on').find('.area_list').find('a.on').text());
            }
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 영화 드로우
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        movieDraw: function (sortVar) {
            var T = this;

            T.movieList.empty();

            var movieNodes = $(scheduleUtill.Movies.Movies.Items);

            if (sortVar) {
                movieNodes.sort(function (a, b) { // 영화 소트
                    return a[sortVar] < b[sortVar] ? -1 : a[sortVar] > b[sortVar] ? 1 : 0;
                });
            } else {
                movieNodes.sort(function (a, b) { // 영화 소트
                    return a.BookingSortSequence < b.BookingSortSequence ? -1 : a.BookingSortSequence > b.BookingSortSequence ? 1 : 0;
                });
            };

            movieNodes.each(function (n) {
                var ageData = $(this)[0].ViewGradeCode;
                var ageView = ageData;
                var movieCode = $(this)[0].RepresentationMovieCode;
                var release = $(this)[0].ReleaseDate;
                var viewingSort = $(this)[0].ViewSortSequence;
                var bookingSort = $(this)[0].BookingSortSequence;
                var poster = $(this)[0].PosterURL;

                if (T.leng == 'EN') {
                    var subjectData = $(this)[0].MovieNameUS;
                } else {
                    var subjectData = $(this)[0].MovieName;
                };

                if (ageData == 00) {
                    ageData = 'all';    // 전체관람가
                };

                // /LCHS/Image/Thum/ticket_no_image01.gif

                html = '<li>';
                html += '<span class="img">';
                html += '<a href="javascript:void(0);"><img src="' + poster + '" alt="' + subjectData + '"></a>';
                html += '</span>';
                html += '<p class="list_text">';
                html += '<a href="javascript:void(0);"><span class="grade_' + ageData + '">' + ageView + '</span>' + subjectData + '</a>';
                html += '</p>';
                html += '</li>';

                T.movieList.append(html).find('li').last().addClass(movieCode).css('left', 188 * n).data({ 'movieID': movieCode });
            });

            if (movieNodes.length > 5) {
                T.movieSum = Math.ceil(movieNodes.length / 5);
            } else {
                T.movie.find('.btn_next').hide();
            };

            T.movie.find('.indicator').empty();
            for (i = 0; i < T.movieSum; i++) {
                var html = '<li><a href="javascript:void(0);">배너' + (i + 1) + '</a></li>';

                T.movie.find('.indicator').append(html);

                if (i == 0) T.movie.find('.indicator').find('li').first().addClass('on');
            };

            T.movieID = cookieJson.ticketingState.schedule.movieId;
            if (T.movieID != '') {
                T.movieList.find('.' + T.movieID).addClass('on');

                var idx = T.movieList.find('.on').index();

                T.movieNo = Math.floor(idx / 5);

                T.movieList.find('li').css({ 'left': '-=' + ((188 * 5) * T.movieNo) });

                T.movie.find('.indicator').find('li').eq(T.movieNo).addClass('on').siblings().removeClass('on');

                if (T.movieNo == 0) {
                    T.movie.find('.btn_prev').hide().next().show();
                } else if (T.movieNo == T.movieSum - 1) {
                    T.movie.find('.btn_prev').show().next().hide();
                } else {
                    T.movie.find('.btn_prev').show().next().show();
                }
            };

            T.cinemaMemory();
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 쿠키에 의한 체크 설정
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        cinemaMemory: function () {
            var T = this;

            var favorite = false;
            if (T.login && scheduleUtill.FavoriteCinemas.Cinemas.Items.length > 0) {
                var favoriteNode = $(scheduleUtill.FavoriteCinemas.Cinemas.Items); //선호영화관 노드

                var favoriteCinema = favoriteNode[0].DivisionCode + '' + Number(favoriteNode[0].DetailDivisionCode) + '' + favoriteNode[0].CinemaID;

                T.areaList.find('.area_btn').removeClass('on').parent().next('.area_cont').hide();
                T.areaList.find('a.' + favoriteCinema).addClass('on').parents('.area_cont').show().prev().find('a').addClass('on');

                $.fn.cookieChanges('add', 'ticketingState.schedule.cinemaId', favoriteNode[0].DivisionCode + '|' + Number(favoriteNode[0].DetailDivisionCode) + '|' + favoriteNode[0].CinemaID);
                T.cinemaID = cookieJson.ticketingState.schedule.cinemaId;
                favorite = true;

            } else {
                T.areaList.find('.area_btn').removeClass('on').parent().next('.area_cont').hide();
                T.areaList.find('a.111016').addClass('on').parents('.area_cont').show().prev().find('a').addClass('on');

                $.fn.cookieChanges('add', 'ticketingState.schedule.cinemaId', '1|1|1016');
                T.cinemaID = cookieJson.ticketingState.schedule.cinemaId;
            };

            if (T.cinemaID != '') {
                T.ticketingPlaySeq(true);
            };

            if (T.movieID != '' && !favorite) {
                T.ticketingPlaySeq(false);
            };

            T.event();
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 이벤트
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event: function () {
            var T = this;

            T.calendar.find('fieldset').on('click', 'input', function () { // 날짜 클릭이벤트
                if (!$(this).next().hasClass('noDate')) {
                    T.date = $(this).next().data('date');
                    T.week = $(this).next().find('span').text();

                    $.fn.cookieChanges('add', 'ticketingState.ticketing.playDate', T.date);
                    $.fn.cookieChanges('add', 'ticketingState.ticketing.playWeek', T.week);

                    if (T.step == 0) {
                        T.ticketingPlaySeq(true);
                    } else {
                        T.ticketingPlaySeq(false);
                    };
                };
            });

            T.calendar.find('> a').on('click', function () { // 날짜 이동
                PAGENO = 1;
                PAGENO2 = 1;

                if (!$(this).hasClass('nodata')) {
                    if (T.calendar.find('fieldset').find('label').is(':animated')) return false;
                    if ($(this).hasClass('prev')) {
                        if (T.dateCurNo > 0) {
                            T.dateCurNo--;
                            T.calendar.find('.calendarArea').find('label').animate({ 'left': '+=' + 840 }, 300);
                            T.calendar.find('fieldset').find('> span').animate({ 'left': '+=' + 840 }, 300);
                        } else {
                            $(this).addClass('nodata');
                            return false;
                        }
                    } else {
                        if (T.dateCurNo < T.dateTotal) {
                            T.dateCurNo++;
                            T.calendar.find('.calendarArea').find('label').animate({ 'left': '-=' + 840 }, 300);
                            T.calendar.find('fieldset').find('> span').animate({ 'left': '-=' + 840 }, 300);
                        } else {
                            $(this).addClass('nodata');
                            return false;
                        };
                    };
                    T.calendarMove();
                };
            });

            T.tabResevation.on('click', 'a', function () {
                
                PAGENO = 1;
                PAGENO2 = 1;

                //var idx = $(this).parent().index();
                //$(this).addClass('on').parent().siblings().find('a').removeClass('on');

                var idx = $(this).parent().parent().index();
                $(this).addClass('on').parent().parent().siblings().find('a').removeClass('on');

                if (idx == 0) { // 진입 체크 '0':영화관별, '1':영화별
                    T.movie.hide();
                    T.movieWrap.hide();
                    T.area.fadeIn(300);
                    T.cineWrap.fadeIn(300);
                } else {
                    T.area.hide();
                    T.cineWrap.hide();
                    T.movie.fadeIn(300);
                    T.movieWrap.fadeIn(300);
                };
                tabIndex = idx; // 2018-08-20 영화관별 상영시간표 선택시에는 Trigger 작동을 피하기 위한 변수값 설정-이상균

                $.fn.cookieChanges('add', 'ticketingState.schedule.tabIdx', idx);
            });

            T.areaList.find('.area_btn').on('click', function () {

                PAGENO = 1;
                PAGENO2 = 1;

                T.areaList.find('.area_btn').removeClass('on').parent().next().hide();
                $(this).addClass('on').parent().next().show();

                // 2016.05.03 웹접근성
                $('#title_h4').text($(this).text());
                $('#title_h5').text('');
            });

            T.areaList.find('.area_cont').on('click', 'a', function () {

                PAGENO = 1;
                PAGENO2 = 1;

                T.areaList.find('.area_cont').find('a').removeClass('on');
                $(this).addClass('on');

                // 2016.05.10 웹접근성
                $('#title_h5').text($(this).text());

                $.fn.cookieChanges('add', 'ticketingState.schedule.cinemaId', $(this).data('cinemaID'));

                T.ticketingPlaySeq(true); // 영화관 선택에 따른 회차정보 호출
            });

            T.movie.on('click', '.btn_prev, .btn_next', function () {

                PAGENO = 1;
                PAGENO2 = 1;

                if (T.movieList.find('li').is(':animated')) return false;

                if ($(this).hasClass('btn_prev')) {
                    if (T.movieNo > 0) {
                        T.movieNo--;
                        T.movieList.find('li').animate({ 'left': '+=' + (188 * 5) }, 300);
                    };
                } else {
                    if (T.movieNo < T.movieSum-1) {
                        T.movieNo++;
                        T.movieList.find('li').animate({ 'left': '-=' + (188 * 5) }, 300);
                    };
                };

                T.movie.find('.indicator').find('li').eq(T.movieNo).addClass('on').siblings().removeClass('on');

                if (T.movieNo == 0) {
                    T.movie.find('.btn_prev').hide().next().show();
                } else if (T.movieNo == T.movieSum - 1) {
                    T.movie.find('.btn_prev').show().next().hide();
                } else {
                    T.movie.find('.btn_prev').show().next().show();
                }
            });


            T.movie.find('.indicator').on('click', 'a', function () {

                PAGENO = 1;
                PAGENO2 = 1;

                var idx = $(this).parent().index();

                if (idx != T.movieNo) {
                    if (T.movieList.find('li').is(':animated')) return false;

                    var idxReturn = 0;
                    if (idx > T.movieNo) {
                        idxReturn = idx - T.movieNo;
                        T.movieList.find('li').animate({ 'left': '-=' + ((188 * 5) * idxReturn) }, 300);
                        T.movieNo = idx;
                    } else {
                        idxReturn = T.movieNo - idx;
                        T.movieList.find('li').animate({ 'left': '+=' + ((188 * 5) * idxReturn) }, 300);
                        T.movieNo = idx;
                    };

                    T.movie.find('.indicator').find('li').eq(T.movieNo).addClass('on').siblings().removeClass('on');

                    if (T.movieNo == 0) {
                        T.movie.find('.btn_prev').hide().next().show();
                    } else if (T.movieNo == T.movieSum - 1) {
                        T.movie.find('.btn_prev').show().next().hide();
                    } else {
                        T.movie.find('.btn_prev').show().next().show();
                    };
                };
            });

            T.movie.find('.item_list').on('click', 'a', function () {

                PAGENO = 1;
                PAGENO2 = 1;

                var idx = $(this).parent().index();

                if (!$(this).hasClass('on')) {
                    $(this).addClass('on').parent().siblings().find('a').removeClass('on');

                    // 2016.05.03 웹접근성
                    if (idx == 0) {
                        $('#title_h3').text('Đặt vé trước'); // 예매순
                        T.movieDraw('BookingSortSequence');
                    } else {
                        $('#title_h3').text('Xếp hạng'); // 평점순
                        T.movieDraw('ViewSortSequence');
                    }
                };
            });

            T.movieList.on('click', 'a', function () {

                PAGENO = 1;
                PAGENO2 = 1;

                if (!$(this).parents('li').hasClass('on')) {
                    $(this).parents('li').addClass('on').siblings().removeClass('on');
                    $.fn.cookieChanges('add', 'ticketingState.schedule.movieId', $(this).parents('li').data('movieID'));

                    T.ticketingPlaySeq(false); // 영화 선택에 따른 회차정보 호출
                };
            });

            T.movieWrapArea.on('click', 'a', function () {

                PAGENO = 1;
                PAGENO2 = 1;

                var idx = $(this).parent().index();
                T.movieWrapArea.find('a').removeClass('on').eq(idx).addClass('on');

                $.fn.cookieChanges('add', 'ticketingState.schedule.movieArea', idx);

                // 2016.05.03 웹접근성
                $('#title_h4_2').text($(this).text());

                if (cookieJson.ticketingState.schedule.movieId != '') {
                    T.ticketingPlaySeq(false);
                };
            });

            if (!T.movieList.find('a').hasClass('on')) {
                if (tabIndex == 1) {
                    // 2018-08-20 영화관별 상영시간표에서 해당 이벤트 호출을 막는다.-이상균
                    T.movieList.find('a').first().trigger('click');
                }
            };

        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 달력 슬라이드 시 이동버튼 콘트롤
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        calendarMove: function () {
            var T = this;

            if (T.dateCurNo == 0) {
                T.calendar.find('.prev').addClass('nodata').siblings('a').removeClass('nodata');
            } else if (T.dateCurNo == T.dateTotal) {
                T.calendar.find('.next').addClass('nodata').siblings('a').removeClass('nodata');
            } else if (T.dateCurNo > 0 && T.dateCurNo < T.dateTotal) {
                T.calendar.find('> a').removeClass('nodata');
            }
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 회차정보 리턴
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        platEvent: function (activeElem) {
            activeElem.find('.time_active').off().on('click', function (e) {
                if ($(this).data('possibilltyYN') == 'N') // 마감
                {
                    e.preventDefault(); // 브라우저 이벤트 리셋
                    return;
                }
                else if ($(this).data('possibilltyYN') == 'J') // 상영준비중
                {
                    e.preventDefault(); // 브라우저 이벤트 리셋
                    return;
                }
                else if ($(this).data('possibilltyYN') == 'E') // 매진
                {
                    e.preventDefault(); // 브라우저 이벤트 리셋
                    return;
                }

                if (!$(this).parent().hasClass('soldout')) {

                    // 슈퍼4D관을 위해 메세지 표시 위한 기능
                    // 0 : 메세지 기능 없음
                    // 1 : 수퍼4D관 2D 영화
                    // 2 : 수퍼4D관 4D 영화
                    // 3 : 무대인사 (HS, MW 해당) 

                    // 무대인사 코드 1603306
                    if ($(this).data('AccompanyTypeCode') == '30') {
                        $(this).data('messageYN', 3);
                    }

                    if (Number($(this).data('messageYN')) > 0) {
                        var serverMessageTxt;
                        if (Number($(this).data('messageYN')) == 1) {
                            serverMessageTxt = 'Hiện tại, phim 2D không có hiệu ứng 4D được hiển thị và Atmos có thể phải trả thêm phí.'; // 현재 4D효과가 없는 2D영화가 상영 중이며, Atmos는 추가요금이 발생할수 있습니다.
                        } else if (Number($(this).data('messageYN')) == 2) {
                            //serverMessageTxt = '4D 영화는 좌석의 움직임이 있는 입체 체감효과와 <br>특수효과가 있어 다음 고객은 관람이 제한됩니다.<br>-임산부/노약자/음주자/심장병, 요통, 고혈압 등의<br> 질환자/신장 100cm 이하 어린이/만 4세미만 <br>(만 4세이상 7세미만은 부모님 동반 하에 관람 가능)';
                            serverMessageTxt = 'Phim 4D có hiệu ứng lập thể và hiệu ứng đặc biệt với chuyển động của ghế ngồi, do đó, những khách hàng sau đây bị giới hạn - Người mang thai / Người cao tuổi / Người uống / Bệnh nhân bị bệnh tim, đau lưng, tăng huyết áp, v.v ... / Trẻ em cao dưới 100cm / Chỉ dưới 4 tuổi và dưới 7 tuổi có thể được nhìn thấy với cha mẹ)';
                        } else if (Number($(this).data('messageYN')) == 3) { // 1603306
                            serverMessageTxt = 'Đặt phòng có thể bị hủy bỏ 24 giờ trước khi bắt đầu bộ phim.'; //원활한 진행을 위해 예매취소는 영화시작<br>24시간 이전까지만 가능합니다.';//20170518 무대 인사 문구 제거 
                        }
                        var viewError = new $.fn.modalPopGeneration({ type: 'server', lang: this.leng, btns: true, btnParam1: false, btnParam2: true, serverTitle: '알림', serverMessage: serverMessageTxt, elem: $(this) });

                        $('.pop_wrap .btnc_confirm').off().on('click', function () {
                            $(this).parents('.pop_wrap').remove();

                            if (memberInfo.MemberNoOn == 'undefined' || memberInfo.MemberNoOn == '' || memberInfo.MemberNoOn == null) {
                                openLoginPopup();
                            } else {

                                // 비호그인시 로그인 팝업 추가 홍상길 - 2015.11.26
                                if (checkLogin() == false) {
                                    openLoginPopupWithParam(undefined, CinemaServerDomain + '/LCHS/Contents/ticketing/person-seat-select.aspx', 'Type01')
                                } else {
                                    window.location.href = CinemaServerDomain + '/LCHS/Contents/ticketing/person-seat-select.aspx';
                                };
                            };
                        });
                    } else {
                        if (memberInfo.MemberNoOn == 'undefined' || memberInfo.MemberNoOn == '' || memberInfo.MemberNoOn == null) {
                            // 월드타워 할로윈 이벤트 20161028/월드타워/닥터 스트레인지/슈퍼플렉스G/20:00
                            var cinemacode = $(this).data('cinemaCode');
                            var moviecode = $(this).data('movieCode');
                            //var specialType = cookieJson.ticketingState.ticketing.playSequenceCode[0].screenDiv;
                            var startTime = $(this).data('startTime');
                            var playDate = $(this).data('playDate');
                            //if (playDate == '2016-10-14' && cinemacode == '1004' && moviecode == '10371' && startTime == '20:55') {
                            if (playDate == '2016-10-28' &&cinemacode == '1016' && moviecode == '10844' && startTime == '20:00') {
                                var popSet = new $.fn.modalPopGeneration({ type: 'confirm', code: 'LBL9999', lang: 'LL', btns: true, btnParam1: true, btnParam2: true, elem: $(this), execution: comfirmExec, execution2: cancelExec });
                                function comfirmExec() {
                                    openLoginPopup();
                                }
                                function cancelExec() {
                                    //T.timeArea.find('.theater_time ').find('li').removeClass('on');
                                }
                            } else {
                                openLoginPopup();
                            }
                        } else {

                            // 월드타워 할로윈 이벤트 20161028/월드타워/닥터 스트레인지/슈퍼플렉스G/20:00
                            var cinemacode = $(this).data('cinemaCode');
                            var moviecode = $(this).data('movieCode');
                            //var specialType = cookieJson.ticketingState.ticketing.playSequenceCode[0].screenDiv;

                            //20170927 추가
                            var specialType = $(this).data('specialCode');                            

                            var startTime = $(this).data('startTime');
                            var playDate = $(this).data('playDate');
                            //if (playDate == '2016-10-14' && cinemacode == '1004' && moviecode == '10371' && startTime == '20:55') {
                            if (playDate == '2016-10-28' &&cinemacode == '1016' && moviecode == '10844' && startTime == '20:00') {
                                var popSet = new $.fn.modalPopGeneration({ type: 'confirm', code: 'LBL9999', lang: 'LL', btns: true, btnParam1: true, btnParam2: true, elem: $(this), execution: comfirmExec, execution2: cancelExec });
                                function comfirmExec() {
                                    // 비호그인시 로그인 팝업 추가 홍상길 - 2015.11.26
                                    if (checkLogin() == false) {
                                        openLoginPopupWithParam(undefined, CinemaServerDomain + '/LCHS/Contents/ticketing/person-seat-select.aspx', 'Type01')
                                    } else {
                                        window.location.href = CinemaServerDomain + '/LCHS/Contents/ticketing/person-seat-select.aspx';
                                    };
                                }
                                function cancelExec() {
                                    //T.timeArea.find('.theater_time ').find('li').removeClass('on');
                                }
                            }
                            else {
                                // 비호그인시 로그인 팝업 추가 홍상길 - 2015.11.26
                                if (checkLogin() == false) {
                                    openLoginPopupWithParam(undefined, CinemaServerDomain + '/LCHS/Contents/ticketing/person-seat-select.aspx', 'Type01')
                                } else {
                                    window.location.href = CinemaServerDomain + '/LCHS/Contents/ticketing/person-seat-select.aspx';
                                };
                            }
                        };

                    }
                   
                    $.fn.cookieChanges('add', 'ticketingState.ticketing.playDate', $(this).data('playDate')); // 1603307
                    //$.fn.cookieChanges('add', 'ticketingState.ticketing.playWeek', $(this).data('playWeek')); // 1603307

                    $.fn.cookieChanges('del', 'ticketingState.ticketing.screenName', 'arr');
                    $.fn.cookieChanges('add', 'ticketingState.ticketing.screenName', $(this).data('screenName'));   // 160607
                    $.fn.cookieChanges('add', 'ticketingState.ticketing.brandName', $(this).data('brandName'));   // 170116
                    $.fn.cookieChanges('del', 'ticketingState.ticketing.playSequenceCode', 'arr');
                    $.fn.cookieChanges('add', 'ticketingState.ticketing.playSequenceCode', { 'cinemaCode': $(this).data('cinemaCode'), 'cinemaName': $(this).data('cinemaName'), 'screenCode': $(this).data('screenCode'), 'movieCode': $(this).data('movieCode'), 'playSequence': $(this).data('playSequence'), 'startTime': $(this).data('startTime'), 'endTime': $(this).data('endTime'), 'sequenceCode': $(this).data('typeCode'), 'weekCode': $(this).data('weekCode'), 'screenDiv': $(this).data('screenDiv') });

                };
            });

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // 영화 상세보기(레이어 모달팝업)
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            this.cineViewList.find(".btn_detail").off().on("click", function () {
                _movieCode = $(this).data("movieCode");
                // 2016.02.25 홍상길 수정 : 새창으로
                goUrl('/LCHS/Contents/Movie/Movie-Detail-View.aspx?movie=' + _movieCode, '2');
                //var popSet = new $.fn.modalPopGeneration({ type: 'load', url: CinemaServerDomain + '/LCHS/View/Movie/movie-detail-popup.html', btns: false, btnParam1: false, btnParam2: false });

            });
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 달력 슬라이드 시 이동버튼 콘트롤
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        calendarMove: function () {
            var R = this;

            if (R.dateCurNo == 0) {
                R.calendar.find('.prev').addClass('nodata').siblings('a').removeClass('nodata');
            } else if (R.dateCurNo == R.dateTotal) {
                R.calendar.find('.next').addClass('nodata').siblings('a').removeClass('nodata');
            } else if (R.dateCurNo > 0 && R.dateCurNo < R.dateTotal) {
                R.calendar.find('> a').removeClass('nodata');
            }
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 회차 데이터 호출
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ticketingPlaySeq: function (bull) {
            if (bull) {
                var obj = {
                    MethodName: "GetPlaySequence"
                    , channelType: "HO"
                    , osType: BrowserAgent()
                    , osVersion: navigator.userAgent
                    , playDate: this.date
                    , cinemaID: cookieJson.ticketingState.schedule.cinemaId
                    , representationMovieCode: ''
                };
                JsonCallSync(CinemaServiceDomain + "/LCWS/Ticketing/TicketingData.aspx", obj, this.playData);
                this.playMovieDraw();

            } else {
                var obj = {
                    MethodName: "GetPlaySequence"
                    , channelType: "HO"
                    , osType: BrowserAgent()
                    , osVersion: navigator.userAgent
                    , playDate: this.date
                    , cinemaID: this.movieWrapArea.find('a').eq(Number(cookieJson.ticketingState.schedule.movieArea)).data('area')
                    , representationMovieCode: cookieJson.ticketingState.schedule.movieId
                };
                JsonCallSync(CinemaServiceDomain + "/LCWS/Ticketing/TicketingData.aspx", obj, this.playData);
                this.playCinemaDraw();

            };
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 회차 데이터 로드
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        playData: function (data) {
            try {
                schedulePlayUtill = data.responseJSON;
            }
            catch (e) {
                //alert('error');
            }
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 회차 정보 디폴트 리셋
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        playReset: function () {
            this.noMovie.hide();

            var drawIdx = this.target.find('.time_inner').find('.select_box').find('select').find('option').first().text();
            var drawData = this.target.find('.time_inner').find('.select_box').find('select').find('option').first().val();
            this.target.find('.time_inner').find('.select_box').find('select').val(drawData).change();
            this.target.find('.time_inner').find('.select_box').find('> a').text(drawIdx);
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 회차 정보 드로우 - 영화
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        playMovieDraw: function () {
            T = this;
            var html = '';

            var cinemaNode = $(schedulePlayUtill.PlaySeqsHeader.Items); // 상영관정보 노드
            var timeNode = $(schedulePlayUtill.PlaySeqs.Items); // 회차 노드
            
            var cinemaTotal = 0;
            var cinemaBull = '', movieBull = '', filmBull = ''; // 영화관순 정렬 변수
            var timeBull = '', cineBull = ''; // 영화순 정렬 변수

            cinemaNode.sort(function (a, b) { // 상영관정보 정렬
                return a.BookingSortSequence < b.BookingSortSequence ? -1 : a.BookingSortSequence > b.BookingSortSequence ? 1 : 0;
            });
            T.cineViewList.find('> dl').remove();

            if (cinemaNode.length == 0) {
                T.cineViewList.find('.time_noData').show();
                return false;
            } else {
                T.cineViewList.find('.time_noData').hide();
            };
            T.cineViewList.find('> dl').remove();

            // 20170725 - 샤롯데/샤롯데프라이빗 표기 수정
            // 샤롯데, 샤롯데/샤롯데프라이빗, 샤롯데프라이빗 형태로 수정
            var tempByMovieCinemaDDArray = new Array();
            var tempByMovieScreenDDArray = new Array();
            var tempByMovieMovieDDArray = new Array();
            var tempByMovieScrrenDDArrayIndex = 0;

            cinemaNode.each(function (n) {

                var cinemaCode = $(this)[0].CinemaID;
                var movieCode = $(this)[0].MovieCode;
                var screenCode = $(this)[0].ScreenID;
                var movieRep = $(this)[0].RepresentationMovieCode;
                var brandYN = $(this)[0].BrandYN;                                                                   // 160628
                if (T.leng == 'EN') {
                    var arrayName = $(this)[0].ScreenNameUS;
                    var cinemaName = $(this)[0].CinemaNameUS;
                    var movieName = $(this)[0].MovieNameUS;
                    var filmName = $(this)[0].FilmNameUS;
                    var viewingName = $(this)[0].ViewGradeNameUS;
                    var soundName = $(this)[0].SoundTypeNameUS;
                    var fourdName = $(this)[0].FourDTypeNameUS;
                    var divisionName = $(this)[0].TranslationDivisionNameUS;
                    var accompanyName = $(this)[0].AccompanyTypeNameUS;
                    var screenName = $(this)[0].ScreenDivisionNameUS;
                    var brandName = $(this)[0].BrandNm_US;
                } else {
                    var arrayName = $(this)[0].ScreenName;
                    var cinemaName = $(this)[0].CinemaName;
                    var movieName = $(this)[0].MovieName;
                    var filmName = $(this)[0].FilmName;
                    var viewingName = $(this)[0].ViewGradeName;
                    var soundName = $(this)[0].SoundTypeName;
                    var fourdName = $(this)[0].FourDTypeName;
                    var divisionName = $(this)[0].TranslationDivisionName;
                    var accompanyName = $(this)[0].AccompanyTypeName;
                    var screenName = $(this)[0].ScreenDivisionName;
                    var brandName = $(this)[0].BrandNm;
                }
                var filmCode = $(this)[0].FilmCode;
                var soundCode = $(this)[0].SoundTypeCode;
                var fourdCode = $(this)[0].FourDTypeCode;
                var divisionCode = $(this)[0].TranslationDivisionCode;
                var accompanyCode = $(this)[0].AccompanyTypeCode;
                var specialCode = $(this)[0].ScreenDivisionCode;
                var translationCode = $(this)[0].TranslationDivisionCode;
                var viewingCode = $(this)[0].ViewGradeCode;
                var totalCode = $(this)[0].TotalSeatCount;

                if (viewingCode == '0') {
                    viewingCode = 'all'; // 전체관람가 변환
                };

                if (!T.cineViewList.find('> dl').hasClass('movie' + movieRep)) { // 1차 "영화관" 정보 드로우
                    html = '<dl class="movie' + movieRep + '">';
                    html += '    <dt>';
                    html += '        <span class="grade_' + viewingCode + '">' + viewingCode + '</span>' + movieName;
                    if (T.leng != 'EN') {
                        html += '        <a href="javascript:void(0)" class="btn_detail" title="Xem thông tin chi tiết phim"><img src="/LCHS/Image/Btn/btn_time_view.png" alt="Chi tiết phòng hát"></a>'; // 새창 열림 , 상영관 상세보기
                    }
                    html += '    </dt>';
                    html += '    <dd>';
                    html += '       <ul class="time_mList">';
                    html += '       </ul>';
                    html += '    </dd>';
                    html += '</dl>';
                    
                    T.cineViewList.append(html).find('.movie' + movieRep).data({ 'movieName': movieName }).find('.btn_detail').data({ 'movieCode': movieRep });
                };

                // 20170719 - 상영시간표 상영관 병합
                var tempByMovieSepcialCode = specialCode;
                var tempByMovieSoundCode = soundCode;

                if (specialCode == 301) {
                    tempByMovieSepcialCode = 300;
                }
                else if (specialCode == 940) {
                    // 수퍼플렉스 -> 일반관
                    tempByMovieSepcialCode = 100;
                }

                // ATMOS 표기 제외, 2D에 포함
                if (soundCode == 300) {
                    tempByMovieSoundCode = 100;
                }

                // 2차 영화관에 대한 영화리스트 드로우(이정열 accompanyCode 수정)
                //if (!T.cineViewList.find('.movie' + movieRep).find('.time_mList').find('> li').hasClass('screen' + filmCode + '' + translationCode + '' + specialCode + '' + soundCode + '' + accompanyCode + '' + brandYN)) {
                // 20170719 - 상영시간표 상영관 병합
                // specialCode -> tempSpecialCode로 변경
                if (!T.cineViewList.find('.movie' + movieRep).find('.time_mList').find('> li').hasClass('screen' + filmCode + '' + translationCode + '' + tempByMovieSepcialCode + '' + tempByMovieSoundCode + '' + accompanyCode + '' + brandYN)) { // 170102
                //if (!T.cineViewList.find('.movie' + movieRep).find('.time_mList').find('> li').hasClass('screen' + filmCode + '' + translationCode + '' + specialCode + '' + accompanyCode)) {
                    //html = '<li class="screen' + filmCode + '' + translationCode + '' + specialCode + '' + soundCode + '' + accompanyCode + '' + brandYN + '">'; // 170102
                    // 20170719 - 상영시간표 상영관 병합
                    // specialCode -> tempSpecialCode로 변경
                    html = '<li class="screen' + filmCode + '' + translationCode + '' + tempByMovieSepcialCode + '' + tempByMovieSoundCode + '' + accompanyCode + '' + brandYN + '">'; // 170102
                    html += '   <ul class="cineD1">';
                    html += '       <li>' + filmName + ' </li>';
                    html += '       <li>' + divisionName + '</li>';

                    // 20170717 - Atmos 표기 제외
                    if (soundCode > 100 && soundCode != 300) {
                        html += '       <li>' + soundName + '</li>';
                    };

                    // 20170719 - 수퍼플렉스 표기 제외
                    if (specialCode > 100 && specialCode != 940) {
                        // 20170719 - 상영시간표 상영관 병합
                        if (specialCode == 300 || specialCode == 301) {
                            // 20170725 - 샤롯데/샤롯데프라이빗 표기 수정
                            // 샤롯데, 샤롯데/샤롯데프라이빗, 샤롯데프라이빗 형태로 수정
                            tempByMovieCinemaDDArray[tempByMovieScrrenDDArrayIndex] = cinemaCode;
                            tempByMovieMovieDDArray[tempByMovieScrrenDDArrayIndex] = movieCode;
                            tempByMovieScreenDDArray[tempByMovieScrrenDDArrayIndex] = 'screen' + filmCode + '' + translationCode + '' + tempByMovieSepcialCode + '' + tempByMovieSoundCode + '' + accompanyCode + '' + brandYN;
                            tempByMovieScrrenDDArrayIndex++;

                            html += '       <li class="pit1"><strong>Charlotte</strong></li>'; // 샤롯데
                        }
                        else {
                            html += '       <li class="pit1">' + screenName + '</li>';
                        }
                    };

                    if (accompanyCode > 10) {
                        html += '       <li>' + accompanyName + '</li>';
                    };
                    // 브랜드관이 아닌 경우 0
                    if (brandYN != '0') {                                                                           // 160628
                        html += '       <li>' + brandName + '</li>';
                    };
                    html += '   </ul>';
                    html += '   <ul class="theater_time list' + movieRep + '" screenDiv="' + specialCode + '">';
                    html += '   </ul>';
                    html += '</li>';

                    T.cineViewList.find('.movie' + movieRep).find('.time_mList').append(html);
                };
            });

            timeNode.sort(function (a, b) { // 회차 정렬
                return a.StartTime < b.StartTime ? -1 : a.StartTime > b.StartTime ? 1 : 0;
            });

            timeNode.each(function (n) { // 3차 각 영화에 대한 회차정보 드로우
                var categoryCode = $(this)[0].CategoryCode;
                var subCode = $(this)[0].SubCode;
                var cinemaCode = $(this)[0].CinemaID;
                var screenCode = $(this)[0].ScreenID;
                var movieCode = $(this)[0].MovieCode;
                var movieRep = $(this)[0].RepresentationMovieCode;
                var startTime = $(this)[0].StartTime;
                var endTime = $(this)[0].EndTime;
                var seatToal = $(this)[0].TotalSeatCount;
                var seatReserved = $(this)[0].BookingSeatCount;
                var typeCode = $(this)[0].SequenceNoGroupCode;
                var possibilltyYN = $(this)[0].IsBookingYN;
                var filmCode = $(this)[0].FilmCode;
                var specialCode = $(this)[0].ScreenDivisionCode;
                var translationCode = $(this)[0].TranslationDivisionCode;
                //이정열 수정
                var accompanyCode = $(this)[0].AccompanyTypeCode;
                var soundCode = $(this)[0].SoundTypeCode;
                var playSequence = $(this)[0].PlaySequence;
                var weekCode = $(this)[0].WeekendDivisionCode;
                var messageYN = $(this)[0].MessageYN; // 1603213
                var playDate = $(this)[0].PlayDt; // 1603306
                var AccompanyTypeCode = $(this)[0].AccompanyTypeCode; // 1603306
                var playDay = $(this)[0].playDay; // 1603307
                var brandYN = $(this)[0].BrandYN; // 1603307

                if (T.leng == 'EN') {
                    var typeName = $(this)[0].SequenceNoGroupNameUS;
                    var screenName = $(this)[0].ScreenNameUS;
                    var soundName = $(this)[0].SoundTypeNameUS;
                    var brandName = $(this)[0].BrandNm_US;      // 170116
                    var cinemaName = $(this)[0].CinemaNameUS;   // 170116
                } else {
                    var typeName = $(this)[0].SequenceNoGroupName;
                    var screenName = $(this)[0].ScreenName;
                    var soundName = $(this)[0].SoundTypeName;
                    var brandName = $(this)[0].BrandNm;      // 170116
                    var cinemaName = $(this)[0].CinemaName;   // 170116
                };

                html = '';

                if (possibilltyYN == 'N' || possibilltyYN == 'E') {
                    html += '<li class="soldout">';
                } else {
                    html += '<li>';
                }
                html += '<a href="javascript:void(0);" class="time_active t' + n + '">';
                if (screenName.length > 4) {                                                                        // 160608
                    html += '   <span class="cineD2 brand">';
                } else {
                    html += '   <span class="cineD2">';
                }


                // // 20170123 - 브랜드관이 아닌 경우 0
                if (brandYN != '0') {                                                                           // 160628
                    // 20170123 - 브랜드관 명칭 표기하지 않음
                    try {
                        var tempScreenNo = screenCode.toString().substr(4, 2);
                        // 20170719 - 상영관 병합, ATmos 관옆에 표기
                        if (soundCode == 300) {
                            html += '       <em>' + Number(tempScreenNo) + '<em> | <em>' + soundName + '</em>';
                        }
                        else {
                            html += '       <em>' + Number(tempScreenNo) + '</em>';
                        }
                    } catch (e) {
                        // 20170719 - 상영관 병합, ATmos 관옆에 표기
                        if (soundCode == 300) {
                            html += '       <em>' + screenName + '<em> | <em>' + soundName + '</em>';
                        }
                        else {
                            html += '       <em>' + screenName + '</em>';
                        }
                    }
                    //html += '       <li>' + brandName + '</li>';
                }
                else {
                    // 20170719 - 상영관 병합, ATmos 관옆에 표기
                    if (soundCode == 300) {
                        html += '       <em>' + screenName + '<em> | <em>' + soundName + '</em>';
                    }
                    else {
                        html += '       <em>' + screenName + '</em>';
                    }
                }

                //html += '       <em>' + screenName + '</em>';
                //if (soundCode > 100) {                161209
                //    html += '       <em>' + soundName + '</em>';
                //};

                html += '</span>';
                switch (typeCode) {
                    case 30:    // 조조
                        html += '<span class="clock2">';
                        html += '<em class="seat iri">' + typeName + '</em>';
                        break;
                    case 90:    // 심야
                        html += '<span class="clock2">';
                        html += '<em class="seat ini">' + typeName + '</em>';
                        break;
                    default:
                        html += '<span class="clock">';
                };

                html += startTime + '<span> ~ ' + endTime + '</span></span>';

                if (possibilltyYN == 'N') {
                    if (T.leng == 'EN') {
                        html += '<span class="ppNum">Finish</span>';
                    } else {
                        html += '<span class="ppNum">Hết hạn đặt</span>'; // 예매마감
                    };
                } else if (possibilltyYN == 'E') {
                    if (T.leng == 'EN') {
                        html += '<span class="ppNum">Finish</span>';
                    } else {
                        html += '<span class="ppNum">Đã bán hết</span>'; // 매진
                    };
                } else if (possibilltyYN == 'J') {
                    if (T.leng == 'EN') {
                        html += '<span class="ppNum">Ready</span>';
                    } else {
                        html += '<span class="ppNum">Chuẩn bị</span>'; // 상영준비중
                    };
                } else {
                    // 20170607 전체 좌석수와 예매된 좌석수가 -1보다 클 경우에만 추가
                    if (seatToal > -1 && seatReserved > -1) {
                        if (T.leng == 'EN') {
                            html += '<span class="ppNum"><em class="color_brown" title="Seats">' + seatReserved + '</em> / ' + seatToal + ' Seat</span>';
                        } else {
                            html += '<span class="ppNum"><em class="color_brown" title="Kiểm tra chỗ ngồi của bạn">' + seatReserved + '</em> / ' + seatToal + ' Ghế ngồi</span>'; //좌석 확인
                        };
                    }
                };

                html += '</a>';
                html += '</li>';

                // 20170719 - 상영시간표 상영관 병합
                var tempByMovieSCDivSpecialCode = specialCode;
                var tempByMovieSCDivSoundCode = soundCode;

                if (specialCode == 301) {
                    // 샤롯데, 샤롯데프라이빗 -> 샤롯데/샤롯데프라이빗으로
                    tempByMovieSCDivSpecialCode = 300;
                }
                else if (specialCode == 940) {
                    // 수퍼플렉스 -> 일반관
                    tempByMovieSCDivSpecialCode = 100;
                }

                if (soundCode == 300) {
                    tempByMovieSCDivSoundCode = 100;
                }


                //var cinemaName = T.cineViewList.find('.movie' + movieRep).data('movieName');  // 170116
                var scDiv = T.cineViewList.find('.movie' + cinemaCode).find('.movie' + movieRep).find('.screen' + filmCode + '' + translationCode + '' + tempByMovieSCDivSpecialCode + '' + tempByMovieSCDivSoundCode + '' + accompanyCode + '' + brandYN).find('.theater_time').attr('screenDiv'); // 170102
                // 1603213
                T.cineViewList.find('.movie' + movieRep).find('.screen' + filmCode + '' + translationCode + '' + tempByMovieSCDivSpecialCode + '' + tempByMovieSCDivSoundCode + '' + accompanyCode + '' + brandYN).find('.list' + movieRep).append(html)
                .find('a').last().data({ 'screenName': screenName, 'brandName': brandName, 'playWeek': playDay, 'playDate': playDate, 'AccompanyTypeCode': AccompanyTypeCode, 'possibilltyYN': possibilltyYN, 'messageYN': messageYN, 'cinemaName': cinemaName, 'cinemaCode': cinemaCode, 'movieCode': movieCode, 'screenCode': screenCode, 'playSequence': playSequence, 'startTime': startTime, 'endTime': endTime, 'typeCode': typeCode, 'weekCode': weekCode, 'screenDiv': scDiv, 'specialCode': specialCode });
            });

            // 20170725 - 샤롯데/샤롯데프라이빗 표기 수정
            // 샤롯데, 샤롯데/샤롯데프라이빗, 샤롯데프라이빗 형태로 수정
            var charlotteByMovieFlag = false;
            var charlotteByMoviePrivateFlag = false;
            for (var i = 0 ; i < tempByMovieScreenDDArray.length ; i++) {

                $('.movie' + tempByMovieMovieDDArray[i]).find($('.' + tempByMovieScreenDDArray[i])).find('a').each(function (n) {
                    if ($(this).data('specialCode') == 300) {
                        charlotteByMovieFlag = true;
                    }

                    if ($(this).data('specialCode') == 301) {
                        charlotteByMoviePrivateFlag = true;
                    }
                });

                if (charlotteByMovieFlag && !charlotteByMoviePrivateFlag) {
                    $('.movie' + tempByMovieMovieDDArray[i]).find($('.' + tempByMovieScreenDDArray[i])).find('.pit1').find('strong').html('Charlotte') // 샤롯데
                }
                else if (!charlotteByMovieFlag && charlotteByMoviePrivateFlag) {
                    $('.movie' + tempByMovieMovieDDArray[i]).find($('.' + tempByMovieScreenDDArray[i])).find('.pit1').find('strong').html('Charlotte Private') // 샤롯데프라이빗
                }
                else if (charlotteByMovieFlag && charlotteByMoviePrivateFlag) {
                    $('.movie' + tempByMovieMovieDDArray[i]).find($('.' + tempByMovieScreenDDArray[i])).find('.pit1').find('strong').html('Charlotte/Charlotte Private') // 샤롯데/샤롯데프라이빗
                }

                charlotteByMovieFlag = false;
                charlotteByMoviePrivateFlag = false;
            }

            //T.cineWrap.find('.theater_time').each(function (n) {
            //    var firstVar = 0;
            //    var lastVar = 0;

            //    for (i = 0; i < Math.round($(this).find('li').length / 4) ; i++) {
            //        firstVar = lastVar + 8;
            //        lastVar = firstVar + 8;
            //        $(this).find('li').slice(firstVar, lastVar).addClass('bg_Btype');
            //    };
            //});
            this.list_view1();
            T.platEvent(T.cineViewList);
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 회차 정보 드로우 - 영화관별
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        playCinemaDraw: function () {
            T = this;
            var html = '';

            var cinemaNode = $(schedulePlayUtill.PlaySeqsHeader.Items); // 상영관정보 노드
            var timeNode = $(schedulePlayUtill.PlaySeqs.Items); // 회차 노드

            cinemaNode.sort(function (a, b) { // 상영관정보 정렬
                return a.CinemaSortSequence < b.CinemaSortSequence ? -1 : a.CinemaSortSequence > b.CinemaSortSequence ? 1 : 0;
            });
            T.movieViewList.find('> dl').remove();

            if (cinemaNode.length == 0) {
                T.movieViewList.find('.time_noData').show();
                return false;
            } else {
                T.movieViewList.find('.time_noData').hide();
            };

            // 20170725 - 샤롯데/샤롯데프라이빗 표기 수정
            // 샤롯데, 샤롯데/샤롯데프라이빗, 샤롯데프라이빗 형태로 수정
            var tempCinemaDDArray = new Array();
            var tempScreenDDArray = new Array();
            var tempMovieDDArray = new Array();
            var tempScrrenDDArrayIndex = 0;

            cinemaNode.each(function (n) {
                var cinemaCode = $(this)[0].CinemaID;
                var movieCode = $(this)[0].MovieCode;
                var screenCode = $(this)[0].ScreenID;
                var movieRep = $(this)[0].RepresentationMovieCode;
                var brandYN = $(this)[0].BrandYN;                                                                   // 160628
                if (T.leng == 'EN') {
                    var arrayName = $(this)[0].ScreenNameUS;
                    var cinemaName = $(this)[0].CinemaNameUS;
                    var movieName = $(this)[0].MovieNameUS;
                    var filmName = $(this)[0].FilmNameUS;
                    var viewingName = $(this)[0].ViewGradeNameUS;
                    var soundName = $(this)[0].SoundTypeNameUS;
                    var fourdName = $(this)[0].FourDTypeNameUS;
                    var divisionName = $(this)[0].TranslationDivisionNameUS;
                    var accompanyName = $(this)[0].AccompanyTypeNameUS;
                    var screenName = $(this)[0].ScreenDivisionNameUS;
                    var brandName = $(this)[0].BrandNm_US;
                } else {
                    var arrayName = $(this)[0].ScreenName;
                    var cinemaName = $(this)[0].CinemaName;
                    var movieName = $(this)[0].MovieName;
                    var filmName = $(this)[0].FilmName;
                    var viewingName = $(this)[0].ViewGradeName;
                    var soundName = $(this)[0].SoundTypeName;
                    var fourdName = $(this)[0].FourDTypeName;
                    var divisionName = $(this)[0].TranslationDivisionName;
                    var accompanyName = $(this)[0].AccompanyTypeName;
                    var screenName = $(this)[0].ScreenDivisionName;
                    var brandName = $(this)[0].BrandNm;
                }
                var filmCode = $(this)[0].FilmCode;
                var soundCode = $(this)[0].SoundTypeCode;
                var fourdCode = $(this)[0].FourDTypeCode;
                var divisionCode = $(this)[0].TranslationDivisionCode;
                var accompanyCode = $(this)[0].AccompanyTypeCode;
                var specialCode = $(this)[0].ScreenDivisionCode;
                var translationCode = $(this)[0].TranslationDivisionCode;
                var viewingCode = $(this)[0].ViewGradeCode;
                var totalCode = $(this)[0].TotalSeatCount;

                if (viewingCode == '0') {
                    viewingCode = 'all'; // 전체관람가 변환
                    if (T.leng == 'EN') {
                        viewingCode = 'all2';
                    }
                } else if (viewingCode == '18') {
                    if (T.leng == 'EN') {
                        viewingCode = '19';
                    }
                };

                if (!T.movieViewList.find('> dl').hasClass('cine' + cinemaCode)) { // 1차 "영화관" 정보 드로우
                    html = '<dl class="cine' + cinemaCode + '">';
                    html += '    <dt>' + cinemaName + '</dt>';
                    html += '    <dd>';
                    html += '       <ul class="time_mList">';
                    html += '       </ul>';
                    html += '    </dd>';
                    html += '</dl>';

                    T.movieViewList.append(html).find('.cine' + cinemaCode).data({ 'cinemaName': cinemaName });
                };

                // 20170719 - 상영시간표 상영관 병합
                var tempSepcialCode = specialCode;
                var tempSoundCode = soundCode;

                if (specialCode == 301) {
                    tempSepcialCode = 300;
                }
                else if (specialCode == 940) {
                    // 수퍼플렉스 -> 일반관
                    tempSepcialCode = 100;
                }

                // ATMOS 표기 제외, 2D에 포함
                if (soundCode == 300) {
                    tempSoundCode = 100;
                }

                // 20170719 - 상영시간표 상영관 병합
                // specialCode -> tempSpecialCode로 변경
                //if (!T.movieViewList.find('.cine' + cinemaCode).find('.time_mList').find('> li').hasClass('screen' + filmCode + '' + translationCode + '' + specialCode + '' + soundCode + '' + accompanyCode + '' + brandYN)) {
                if (!T.movieViewList.find('.cine' + cinemaCode).find('.time_mList').find('> li').hasClass('screen' + filmCode + '' + translationCode + '' + tempSepcialCode + '' + tempSoundCode + '' + accompanyCode + '' + brandYN)) {  // 170102
                    // 20170719 - 상영시간표 상영관 병합
                    // specialCode -> tempSpecialCode로 변경
                    //html = '<li class="screen' + filmCode + '' + translationCode + '' + specialCode + '' + soundCode + '' + accompanyCode + '' + brandYN + '">'; // 170102
                    html = '<li class="screen' + filmCode + '' + translationCode + '' + tempSepcialCode + '' + tempSoundCode + '' + accompanyCode + '' + brandYN + '">'; // 170102
                    html += '   <ul class="cineD1">';
                    html += '       <li>' + filmName + ' </li>';
                    html += '       <li>' + divisionName + '</li>';
                    // 20170717 - Atmos 표기 제외
                    if (soundCode > 100 && soundCode != 300) {
                        html += '       <li>' + soundName + '</li>';
                    };
                    // 20170719 - 수퍼플렉스 표기 제외
                    if (specialCode > 100 && specialCode != 940) {
                        // 20170719 - 상영시간표 상영관 병합
                        if (specialCode == 300 || specialCode == 301) {
                            // 20170725 - 샤롯데/샤롯데프라이빗 표기 수정
                            // 샤롯데, 샤롯데/샤롯데프라이빗, 샤롯데프라이빗 형태로 수정
                            tempCinemaDDArray[tempScrrenDDArrayIndex] = cinemaCode;
                            tempMovieDDArray[tempScrrenDDArrayIndex] = movieCode;
                            tempScreenDDArray[tempScrrenDDArrayIndex] = 'screen' + filmCode + '' + translationCode + '' + tempSepcialCode + '' + tempSoundCode + '' + accompanyCode + '' + brandYN;
                            tempScrrenDDArrayIndex++;

                            //html += '       <li class="pit1"><strong>샤롯데/샤롯데프라이빗</strong></li>';
                            html += '       <li class="pit1"><strong>Charlotte</strong></li>'; // 샤롯데
                        }
                        else {
                            html += '       <li class="pit1">' + screenName + '</li>';
                        }
                    };
                    if (accompanyCode > 10) {
                        html += '       <li>' + accompanyName + '</li>';
                    };
                    // 브랜드관이 아닌 경우 0
                    if (brandYN != '0') {                                                                           // 160628
                        html += '       <li>' + brandName + '</li>';
                    };
                    html += '   </ul>';
                    html += '   <ul class="theater_time list' + movieRep + '" screenDiv="' + specialCode + '">';
                    html += '   </ul>';
                    html += '</li>';

                    T.movieViewList.find('.cine' + cinemaCode).find('.time_mList').append(html);
                };
            });

            timeNode.sort(function (a, b) { // 회차 정렬
                return a.StartTime < b.StartTime ? -1 : a.StartTime > b.StartTime ? 1 : 0;
            });

            timeNode.each(function (n) { // 3차 각 영화에 대한 회차정보 드로우
                var categoryCode = $(this)[0].CategoryCode;
                var subCode = $(this)[0].SubCode;
                var cinemaCode = $(this)[0].CinemaID;
                var screenCode = $(this)[0].ScreenID;
                var movieCode = $(this)[0].MovieCode;
                var movieRep = $(this)[0].RepresentationMovieCode;
                var startTime = $(this)[0].StartTime;
                var endTime = $(this)[0].EndTime;
                var seatToal = $(this)[0].TotalSeatCount;
                var seatReserved = $(this)[0].BookingSeatCount;
                var typeCode = $(this)[0].SequenceNoGroupCode;
                var possibilltyYN = $(this)[0].IsBookingYN;
                var filmCode = $(this)[0].FilmCode;
                var specialCode = $(this)[0].ScreenDivisionCode;
                var translationCode = $(this)[0].TranslationDivisionCode;
                var accompanyCode = $(this)[0].AccompanyTypeCode;
                var soundCode = $(this)[0].SoundTypeCode;
                var playSequence = $(this)[0].PlaySequence;
                var weekCode = $(this)[0].WeekendDivisionCode;
                var messageYN = $(this)[0].MessageYN; // 1603213
                var playDate = $(this)[0].PlayDt; // 1603306
                var AccompanyTypeCode = $(this)[0].AccompanyTypeCode; // 1603306
                var playDay = $(this)[0].playDay; // 1603307
                var brandYN = $(this)[0].BrandYN;                                                                   // 160628
                if (T.leng == 'EN') {
                    var typeName = $(this)[0].SequenceNoGroupNameUS;
                    var screenName = $(this)[0].ScreenNameUS;
                    var soundName = $(this)[0].SoundTypeNameUS;
                    var brandName = $(this)[0].BrandNm_US;      // 170116
                    var cinemaName = $(this)[0].CinemaNameUS;   // 170116
                } else {
                    var typeName = $(this)[0].SequenceNoGroupName;
                    var screenName = $(this)[0].ScreenName;
                    var soundName = $(this)[0].SoundTypeName;
                    var brandName = $(this)[0].BrandNm;      // 170116
                    var cinemaName = $(this)[0].CinemaName;   // 170116
                };

                if (parseInt(screenCode, 10) < 10) screenCode = "0" + screenCode;

                html = '';

                if (possibilltyYN == 'N' || possibilltyYN == 'E') {
                    html += '<li class="soldout">';
                } else {
                    html += '<li>';
                }
                html += '<a href="javascript:void(0);" class="time_active t' + n + '">';
                if (screenName.length > 4) {                                                                        // 160608
                    html += '   <span class="cineD2 brand">';
                } else {
                    html += '   <span class="cineD2">';
                }
                //html += '       <em>' + screenName + '</em>';

                // // 20170123 - 브랜드관이 아닌 경우 0
                if (brandYN != '0') {                                                                           // 160628
                    // 20170123 - 브랜드관 명칭 표기하지 않음
                    try {
                        var tempScreenNo = screenCode.toString().substr(4, 2);
                        // 20170719 - 상영관 병합, ATmos 관옆에 표기
                        if (soundCode == 300) {
                            html += '       <em>' + Number(tempScreenNo) + 'Screen<em> | <em>' + soundName + '</em>';
                        }
                        else {
                            html += '       <em>' + Number(tempScreenNo) + 'Screen</em>';
                        }
                    } catch (e) {
                        // 20170719 - 상영관 병합, ATmos 관옆에 표기
                        if (soundCode == 300) {
                            html += '       <em>' + screenName + '<em> | <em>' + soundName + '</em>';
                        }
                        else {
                            html += '       <em>' + screenName + '</em>';
                        }
                    }
                    //html += '       <li>' + brandName + '</li>';
                }
                else {
                    // 20170719 - 상영관 병합, ATmos 관옆에 표기
                    if (soundCode == 300) {
                        html += '       <em>' + screenName + '<em> | <em>' + soundName + '</em>';
                    }
                    else {
                        html += '       <em>' + screenName + '</em>';
                    }
                       
                }

                //if (soundCode > 100) {        // 161209
                //    html += '       <em>' + soundName + '</em>';
                //};
                html += '</span>';
                switch (typeCode) {
                    case 30:    // 조조
                        html += '<span class="clock2">';
                        html += '<em class="seat iri">' + typeName + '</em>';
                        break;
                    case 90:    // 심야
                        html += '<span class="clock2">';
                        html += '<em class="seat ini">' + typeName + '</em>';
                        break;
                    default:
                        html += '<span class="clock">';
                };

               html += startTime + '<span> ~ ' + endTime + '</span></span>';

                if (possibilltyYN == 'N') {
                    if (T.leng == 'EN') {
                        html += '<span class="ppNum">Finish</span>';
                    } else {
                        html += '<span class="ppNum">Hết hạn đặt</span>'; // 예매마감
                    };
                } else if (possibilltyYN == 'E') {
                    if (T.leng == 'EN') {
                        html += '<span class="ppNum">Finish</span>';
                    } else {
                        html += '<span class="ppNum">Đã bán hết</span>'; // 매진
                    };
                } else if (possibilltyYN == 'J') {
                    if (T.leng == 'EN') {
                        html += '<span class="ppNum">Ready</span>';
                    } else {
                        html += '<span class="ppNum">Chuẩn bị</span>'; // 상영준비중
                    };
                } else {
                    if (T.leng == 'EN') {
                        html += '<span class="ppNum"><em class="color_brown" title="Seats">' + seatReserved + '</em> / ' + seatToal + ' Seats</span>';
                    } else {
                        html += '<span class="ppNum"><em class="color_brown" title="Kiểm tra chỗ ngồi của bạn">' + seatReserved + '</em> / ' + seatToal + ' Ghế ngồi</span>'; // 좌석 확인
                    };
                };

                html += '</a>';
                html += '</li>';

                // 20170719 - 상영시간표 상영관 병합
                var tempSCDivSpecialCode = specialCode;
                var tempSCDivSoundCode = soundCode;

                if (specialCode == 301) {
                    // 샤롯데, 샤롯데프라이빗 -> 샤롯데/샤롯데프라이빗으로
                    tempSCDivSpecialCode = 300;
                }
                else if (specialCode == 940) {
                    // 수퍼플렉스 -> 일반관
                    tempSCDivSpecialCode = 100;
                }

                if (soundCode == 300) {
                    tempSCDivSoundCode = 100;
                }

                //var cinemaName = T.movieViewList.find('.cine' + cinemaCode).data('cinemaName');  // 170116
                var scDiv = T.movieViewList.find('.cine' + cinemaCode).find('.screen' + filmCode + '' + translationCode + '' + tempSCDivSpecialCode + '' + tempSCDivSoundCode + '' + accompanyCode + '' + brandYN).find('.theater_time').attr('screenDiv'); // 170102
                // 1603306
                T.movieViewList.find('.cine' + cinemaCode).find('.screen' + filmCode + '' + translationCode + '' + tempSCDivSpecialCode + '' + tempSCDivSoundCode + '' + accompanyCode + '' + brandYN).find('.theater_time').append(html)
                .find('a').last().data({ 'screenName': screenName, 'brandName': brandName, 'playWeek': playDay, 'playDate': playDate, 'AccompanyTypeCode': AccompanyTypeCode, 'possibilltyYN': possibilltyYN, 'messageYN': messageYN, 'cinemaName': cinemaName, 'cinemaCode': cinemaCode, 'movieCode': movieCode, 'screenCode': screenCode, 'playSequence': playSequence, 'startTime': startTime, 'endTime': endTime, 'typeCode': typeCode, 'weekCode': weekCode, 'screenDiv': scDiv, 'specialCode': specialCode });
            });

            // 20170725 - 샤롯데/샤롯데프라이빗 표기 수정
            // 샤롯데, 샤롯데/샤롯데프라이빗, 샤롯데프라이빗 형태로 수정
            var charlotteFlag = false;
            var charlottePrivateFlag = false;
            for (var i = 0 ; i < tempScreenDDArray.length ; i++) {

                $('.cine' + tempCinemaDDArray[i]).find($('.' + tempScreenDDArray[i])).find('a').each(function (n) {
                    if ($(this).data('specialCode') == 300) {
                        charlotteFlag = true;
                    }

                    if ($(this).data('specialCode') == 301) {
                        charlottePrivateFlag = true;
                    }
                });

                if (charlotteFlag && !charlottePrivateFlag) {
                    $('.cine' + tempCinemaDDArray[i]).find($('.' + tempScreenDDArray[i])).find('.pit1').find('strong').html('Charlotte') // 샤롯데
                }
                else if (!charlotteFlag && charlottePrivateFlag) {
                    $('.cine' + tempCinemaDDArray[i]).find($('.' + tempScreenDDArray[i])).find('.pit1').find('strong').html('Charlotte Private') // 샤롯데프라이빗
                }
                else if (charlotteFlag && charlottePrivateFlag) {
                    $('.cine' + tempCinemaDDArray[i]).find($('.' + tempScreenDDArray[i])).find('.pit1').find('strong').html('Charlotte/Charlotte Private') // 샤롯데/샤롯데프라이빗
                }

                charlotteFlag = false;
                charlottePrivateFlag = false;
            }

            //T.movieWrap.find('.theater_time').each(function (n) {
            //    var firstVar = 0;
            //    var lastVar = 0;

            //    for (i = 0; i < Math.round($(this).find('li').length / 4) ; i++) {
            //        firstVar = lastVar + 8;
            //        lastVar = firstVar + 8;
            //        $(this).find('li').slice(firstVar, lastVar).addClass('bg_Btype');
            //    };
            //});
            this.list_view2();
            T.platEvent(T.movieViewList);
        },
        list_view1: function () {

            var max_view = BLOCKSIZE * PAGENO;

            var i = 0;

            T.cineViewList.find('> dl').each(function (index) {
                ++i;
                if (index >= max_view) {
                    T.cineViewList.find('> dl').eq(index).addClass('displays');
                }
            });
           
            if (max_view >= i) {
                $("#aMore2").hide();
                $("#aMore3").hide();
            } else {
                $("#aMore2").show();
                $("#aMore3").show();
            }

            if (PAGENO == 1 && list_view1_cnt == 1) {
                //더보기 버튼 이벤트 설정
                $("#aMore2").click(function () {
                    ++PAGENO;
                    T.playMovieDraw();
                });
            }

            ++list_view1_cnt;

        },
        list_view2: function () {

            var max_view2 = BLOCKSIZE2 * PAGENO2;

            var i = 0;

            T.movieViewList.find('> dl').each(function (index) {
                ++i;
                if (index >= max_view2) {
                    T.movieViewList.find('> dl').eq(index).addClass('displays');
                }
            });
            
            if (max_view2 >= i) {
                $("#aMore2").hide();
                $("#aMore3").hide();
            } else {
                $("#aMore2").show();
                $("#aMore3").show();
            }

            if (PAGENO2 == 1 && list_view2_cnt == 1) {
                //더보기 버튼 이벤트 설정
                $("#aMore3").click(function () {
                    ++PAGENO2;
                    T.playCinemaDraw();
                });
            }

            ++list_view2_cnt;
        },

    };
});
