
/*************************************************************************************************
 *  @Author : 이인수 (is@madive.co.kr)
 *  @Description : web 이벤트 관리
 *  @Version  1.0.8
 ************************************************************************************************/

var adPicPos = 243 + $('#wrap > .ad-banner').outerHeight();
var quickPos = 0;


var adMovie;
$("#mainvisual a").click(function () {
    adMovie = $(this);


});

$("#mainvisual p").click(function () {
    adMovie = $(this);
});


$(function () {

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 링크 해시 리셋
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $.fn.linkHashChange = function () {
        $('a').each(function () {
            if ($(this).attr('href') == '#') $(this).attr('href', 'javascript:void(0);');
        });
    };
    $.fn.linkHashChange();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 푸터 패밀리 사이트
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.footer_box > a').on('click', function () {
        $(this).next().slideToggle(300);
    });
    $('.footer_box').on('mouseleave', function () {
        $(this).find('> ul').slideUp(300);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 좌측 광고영역
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    var quickTimer = setInterval(function () {
        if ($('#content').find('div').hasClass('sub_visual')) {
            quickPos = $('#content').find('.sub_visual').outerHeight();
            adPicPos = quickPos + 243 + $('#wrap > .ad-banner').outerHeight();
        };

        if ($('#content').find('div').hasClass('movie_trailer')) {
            quickPos = $('#content').find('.movie_trailer').outerHeight();
            adPicPos = quickPos + 243 + $('#wrap > .ad-banner').outerHeight();
        };

        if ($('#content').find('div').hasClass('special_box')) {
            quickPos = $('#content').find('.special_box').outerHeight() + $('#content').find('.tab_sWrap').outerHeight();
            adPicPos = quickPos + 288 + $('#wrap > .ad-banner').outerHeight();
        };

        $('#quick').stop().animate({ 'top': quickPos }, 100);
        $('.ad_pic').stop().animate({ 'top': adPicPos }, 100);
    }, 50);

    $(window).scroll(function () {
        clearInterval(quickTimer);
        var wrapHeight = $('#wrap').height();
        var winStop = $(window).scrollTop();
        var footHeight = $('#footer').height();
        var acHeight = $('.ad_pic').height();
        var quickHeight = $('#quick').height();

        if (adPicPos < winStop) {
            $('.ad_pic').stop().animate({ 'top': winStop + (acHeight - 10) }, 700);
        } else if (adPicPos > winStop) {
            $('.ad_pic').stop().animate({ 'top': adPicPos }, 700);
        }

        if (quickPos < winStop) {
            $('#quick').stop().animate({ 'top': winStop }, 700);
        } else if (quickPos + 10 >= winStop) {
            $('#quick').stop().animate({ 'top': quickPos }, 700);
        }

    });
    // 상단 온라인광고 닫기 및 플로팅 배너 위치
    $('.btn_close').on('click', function (event) {
        clearInterval(quickTimer);
        adPicPos = quickPos + 323 - $('#wrap > .ad-banner').outerHeight();

        if ($('#content').find('div').hasClass('sub_visual')) {
            quickPos = $('#content').find('.sub_visual').outerHeight() + 30 + $('#content').find('.tab_sWrap').outerHeight();
            adPicPos = quickPos + 288 - $('#wrap > .ad-banner').outerHeight();
        };

        if ($('#content').find('div').hasClass('movie_trailer')) {
            quickPos = $('#content').find('.movie_trailer').outerHeight() + 30 + $('#content').find('.tab_sWrap').outerHeight();
            adPicPos = quickPos + 288 - $('#wrap > .ad-banner').outerHeight();
        };

        if ($('#content').find('div').hasClass('special_box')) {
            quickPos = $('#content').find('.special_box').outerHeight() + $('#content').find('.tab_sWrap').outerHeight() + 30;
            adPicPos = quickPos + 323 - $('#wrap > .ad-banner').outerHeight();
        };
        event.preventDefault();
        $(this).closest('.ad-banner').hide();
        $('.ad_pic').stop().animate({ 'top': adPicPos }, 0);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 메인키비쥬얼
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    var visualView = 2;
    $.fn.keyVisualSet = function (option) { // 메인키비쥬얼 셋팅

        var S = $.extend({ target: null, event: '.event', trailer: '.trailer', item: 'li', slide: false, prev: '.prev', next: '.next', speed: 500, page: '.paging' }, option);

        /*****
			{target:슬라이드 타겟 클래스, event:이벤트 클래스 네임, trailer:트레일러 클래스 네임, item:슬라이드 엘리먼트, slide:현재 작동 슬라이드, prev:이전 버튼 클래스, next:다음 버튼 클래스, speed:애니메이션 속도, page:페이징 생성 클래스}
		*****/

        this.target = $(S.target);
        this.elem = this.target.find('> div');
        this.slide = S.slide;
        this.item = S.item;
        this.event = this.target.find(S.event);
        this.trailer = this.target.find(S.trailer);
        this.office = this.target.find('.office');
        this.itemA = this.event.find(S.item);
        this.itemB = this.trailer.find(S.item);

        this.itemList = this.target.find('.officeRk');

        this.prev = this.target.find(S.prev);
        this.next = this.target.find(S.next);

        this.minWidth = 0;
        this.maxWidth = 0;

        this.curNo = new Array(0, 0);
        this.total = new Array(this.itemA.length - 1, this.itemB.length - 1);
        this.itemW = new Array(this.itemA.width(), this.itemB.width());
        this.way = 1;
        this.tabSpeed = S.speed;
        this.speed = S.stabSpeed;

        this.visualUtill = '';

        this.init(); // 시작함수
    };

    $.fn.keyVisualSet.prototype = { // 메인키비쥬얼 프로토타입
        init: function () {
            if (this.slide) {
                this.prev.fadeIn(300);
                if (this.itemA.length == 1) {
                    this.prev.fadeOut(300);
                };
            } else {
                this.prev.fadeIn(300);
                if (this.itemB.length == 1) {
                    this.next.fadeOut(300);
                };
            };

            this.reset();
        },

        selectEvent: function () {
            var S = this;

            S.target.on('click', '.prev, .next', function (e) { // 이동버튼 이벤트
                S.way = S.target.find('> a').index(this); // 현재 활성화 화면

                if (!S.target.find(S.item).is(':animated')) { // 애니메이트 도중 클릭 방지
                    S.update();
                };
            });

            S.elem.find('a').on('focusin', function () { // 탭이동을 위한 포커스 체크 이벤트
                focusIdx = $(this).parent().index();
            });
            S.itemB.last().find('a').on('focusout', function () {
                focusIdx = null;
            });

            S.target.on('keyup', function (e) { // 탭이동 이벤트
                if (S.elem.find('a:focus').parents('div').attr('class') == S.event.attr('class')) { // 클래스 추출 후 해당 이벤트 설정
                    S.way = 0;
                } else if (S.elem.find('a:focus').parents('div').attr('class') == S.trailer.attr('class')) {
                    S.way = 1;
                };

                if (!e.shiftKey && e.keyCode == 9) { // 탭이동 정방향
                    var elemIdx = S.event.position();
                    if (elemIdx.left < 0 && S.way != 1) { // 이벤트 및 트레일러 활성화
                        S.slide = true;
                        S.speed = 50;
                        S.align();
                    } else if (elemIdx.left > -1 && S.way == 1) {
                        S.slide = false;
                        S.speed = 50;
                        S.align();
                    };
                    if (focusIdx > 0) { // 개별 슬라이드 진행
                        S.speed = 50;
                        (S.way != 1) ? S.prev.trigger('click') : S.next.trigger('click');
                    };
                } else if (e.shiftKey && e.keyCode == 9) { // 탭이동 역방향
                    if (S.elem.find('a:focus').parents('div').attr('class') == S.event.attr('class') || S.elem.find('a:focus').parents('div').attr('class') == S.trailer.attr('class')) {
                        var elemIdx = S.event.position();
                        if (S.way != 1) { // 이벤트 및 트레일러 활성화
                            if (elemIdx.left < 0) {
                                S.slide = true;
                                S.speed = 50;
                                S.align();
                            };
                            S.curNo[S.way] = focusIdx;
                            S.itemA.each(function (n) { // 해당 슬라이드 재정렬
                                if (S.curNo[S.way] >= n) {
                                    $(this).css({ 'right': (S.curNo[S.way] - n) * S.itemW[0] });
                                } else {
                                    $(this).css({ 'right': (n - S.curNo[S.way]) * S.itemW[0] });
                                };
                            });
                        } else {
                            if (elemIdx.left > -1) {
                                S.slide = false;
                                S.speed = 50;
                                S.align();
                            };
                            S.curNo[S.way] = focusIdx;
                            S.itemB.each(function (n) { // 해당 슬라이드 재정렬
                                if (S.curNo[S.way] >= n) {
                                    $(this).css({ 'left': (S.curNo[S.way] - n) * S.itemW[0] });
                                } else {
                                    $(this).css({ 'left': (n - S.curNo[S.way]) * S.itemW[0] });
                                };
                            });
                        };
                    };
                };
            });
        },

        reset: function () { // 슬라이드 초기화
            X = this;
            X.itemA.each(function (n) {
                $(this).css({ 'right': n * X.itemW[0] });
            });
            X.itemB.each(function (n) {
                $(this).css({ 'left': n * X.itemW[1] });
            });

            X.selectEvent();
        },

        sizeSet: function () {
            var T = this;

            var winWidth = $(window).width();
            var tPos = T.trailer.offset();

            T.minWidth = winWidth - tPos.left;
            T.maxWidth = winWidth - (T.minWidth + 335);

            T.trailer.css('width', T.minWidth);
            T.event.css('width', T.maxWidth);
        },

        update: function () { // 현재 슬라이드 정보 업데이트 후 애니메이션 호출
            if (this.way != 1) {
                if (this.slide) {
                    (this.total[this.way] > this.curNo[this.way]) ? this.curNo[this.way]++ : this.curNo[this.way] = 0;
                    this.animate();
                } else {
                    this.slide = true;
                    this.align();
                };
            } else {
                if (!this.slide) {
                    (this.total[this.way] > this.curNo[this.way]) ? this.curNo[this.way]++ : this.curNo[this.way] = 0;
                    this.animate();
                } else {
                    this.slide = false;
                    this.align();
                };
            };
        },

        align: function () { // 이벤트 및 트레일러 활성화
            var T = this;

            if (this.slide) {
                var insA = '+=';
                T.event.stop().animate({ 'right': insA + (-685) }, T.speed, 'easeOutExpo');
                T.trailer.stop().animate({ 'left': insA + 685 }, T.speed, 'easeOutExpo');
                T.office.stop().animate({ 'left': insA + 685 }, T.speed, 'easeOutExpo');

                T.next.fadeIn(300);
                if (T.itemA.length == 1) {
                    T.prev.fadeOut(300);
                };
            } else {
                var insA = '-=';

                T.event.stop().animate({ 'right': insA + (-685) }, T.speed, 'easeOutExpo');
                T.trailer.stop().animate({ 'left': insA + 685 }, T.speed, 'easeOutExpo');
                T.office.stop().animate({ 'left': insA + 685 }, T.speed, 'easeOutExpo');


                T.prev.fadeIn(300);
                if (T.itemB.length == 1) {
                    T.next.fadeOut(300);
                };
            };
        },

        animate: function () { // 슬라이드 에니메이션
            var T = this;

            if (T.way != 1) { // T.way에 따른 이벤트, 트레일러 선택 슬라이드
                if (T.itemA.length > 1) {
                    T.itemA.animate({ 'right': '-=' + T.itemW[T.way] }, T.speed, 'easeOutExpo',
                    function () {
                        (T.curNo[T.way] != 0) ? T.itemA.eq(T.curNo[T.way] - 1).css({ 'right': T.total[T.way] * T.itemW[T.way] }) : T.itemA.eq(T.total[T.way]).css({ 'right': T.total[T.way] * T.itemW[T.way] });
                    });
                };
            } else {
                if (T.itemB.length > 1) {
                    T.itemB.animate({ 'left': '-=' + T.itemW[T.way] }, T.speed, 'easeOutExpo',
                    function () {
                        (T.curNo[T.way] != 0) ? T.itemB.eq(T.curNo[T.way] - 1).css({ 'left': T.total[T.way] * T.itemW[T.way] }) : T.itemB.eq(T.total[T.way]).css({ 'left': T.total[T.way] * T.itemW[T.way] });
                    });
                };
            };
        }
    };

    //var keyVisualSet = new $.fn.keyVisualSet({target:'.main_key_visual', event:'.event', trailer:'.trailer', speed:300}); // 함수 호출 방식

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 슬라이드 및 페이드
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $.fn.slideGeneration = function (option) {
        var S = $.extend({ target: null, box: '.slide', item: 'li', bundle: 1, space: 0, animation: 'slide', type: 'easeOutExpo', btn: true, prev: '.btnc_prev', next: '.btnc_next', speed: 300, timer: false, timerPlay: '.rolling_play', timerStop: '.rolling_stop', delay: 3000, page: true, pageItem: '.indicator' }, option);

        /*****
			{target:애니메이션 타겟 클래스, box:이벤트 클래스 네임, item:슬라이드 엘리먼트, bundle:묶음개수, space: animation:애니메이션 형태, prev:이전 버튼 클래스, next:다음 버튼 클래스, speed:애니메이션 속도,
			timer:타이머 여부, delay:타이머 간격, page:페이징여부, pageItem:페이징 클래스}
		*****/

        this.target = $(S.target);
        this.box = this.target.find(S.box);
        this.item = this.box.find(S.item);
        this.bundle = S.bundle;
        this.animation = S.animation;
        this.type = S.type;
        this.timer = S.timer;
        if (this.timer) {
            this.thisTimer;
            this.condition = true;
            this.play = this.target.find(S.timerPlay);
            this.stop = this.target.find(S.timerStop);
        };
        this.delay = S.delay;
        this.btn = S.btn;

        if (S.btn) {
            this.prev = this.target.find(S.prev);
            this.next = this.target.find(S.next);
        };

        this.curNo = 0;
        this.space = S.space;
        this.itemW = this.item.outerWidth() + this.space;
        this.speed = S.speed;

        if (this.box.outerWidth() > this.itemW) {
            this.itemAbat = Math.ceil(this.box.outerWidth() / (((this.itemW + this.space) * this.bundle))) - (this.bundle);
        } else {
            this.itemAbat = 0;
        };

        if (this.box.attr('class') == 'bannerImg') {
            this.item.each(function () {
                if (!$(this).find('a').hasClass('btnc_play')) {
                    var data = $(this).data();
                    $(this).css('background', 'url("' + data.ImagePath + '") no-repeat 50% 0');

                    if ($(this).find('a').length > 0)
                        $(this).find('a').append('<span class="blind">' + data.ImageAlt + '</span>');
                    else
                        $(this).append('<span class="blind">' + data.ImageAlt + '</span>');
                };
            });
        };

        this.total = (this.item.length - 1) - Math.abs(this.itemAbat);

        this.page = S.page;
        if (S.page) {
            this.pageItem = this.target.find(S.pageItem);
        };

        if (this.bundle == 1) {
            if (S.btn) this.prev.fadeIn(this.speed);
        };

        this.init();
    };

    $.fn.slideGeneration.prototype = {
        init: function () {
            var Z = this;

            if (Z.animation == 'slide') { // 정렬
                Z.item.each(function (n) {
                    $(this).css({ 'left': Z.itemW * n });
                });
            };

            if (Z.total < 1) {
                Z.prev.hide();
                Z.next.hide();
                if (this.timer) {
                    Z.play.hide();
                    Z.stop.hide();
                };
                return false;
            };

            if (Z.btn) { // 버튼 이벤트
                this.prev.hide();

                this.prev.on('click', function () {
                    if (Z.item.is(':animated')) return false;
                    if (Z.animation == 'fade' && Z.curNo == 0) {
                        Z.curNo = Z.total;
                        Z.btnAction();
                    } else if (Z.curNo > 0) {
                        Z.curNo--;
                        Z.btnAction('al', false);
                    };
                    Z.update();
                });
                this.next.on('click', function () {
                    if (Z.item.is(':animated')) return false;
                    if (Z.animation == 'fade' && Z.curNo == Z.total) {
                        Z.curNo = 0;
                        Z.btnAction();
                    } else if (Z.curNo < Z.total) {
                        Z.curNo++;
                        Z.btnAction('ar', false);
                    };
                    Z.update();
                });
            };

            if (Z.page) { // 페이징 생성
                Z.item.each(function (n) {
                    Z.pageItem.append('<li><a href="#' + (n + 1) + '">' + (n + 1) + '</a></li>');
                    if (n == 0) Z.pageItem.find('li').eq(n).addClass('on');
                });

                Z.pageItem.on('click', 'a', function () { // 페이징 클릭
                    if (Z.item.is(':animated')) return false;

                    var index = $(this).parent().index();
                    Z.pageAction(index, 'pa');
                    Z.update();
                });
            };

            if (Z.timer) { // 타이머 - true 일 경우
                Z.settime();
                Z.condition = true;

                Z.play.on('click', function () {
                    if (Z.condition) return false;
                    Z.condition = true;
                    clearInterval(Z.thisTimer);
                    Z.settime();
                });
                Z.stop.on('click', function () {
                    clearInterval(Z.thisTimer);
                    Z.condition = false;
                });
            };

            $(window).on('resize', function () {
                Z.itemW = Z.item.outerWidth() + Z.space;
            });
        },

        btnAction: function (align, bull) { // 좌우 버튼 클릭
            var way;
            var D = this;

            if (D.animation == 'slide') {
                (align != 'ar') ? way = '+=' : way = '-=';

                if (bull) {
                    D.item.eq(D.curNo).show().css({ 'left': D.itemW });
                    D.item.animate({ 'left': way + D.itemW }, D.speed, D.type, function () {
                        if (D.bundle == 1) {
                            if (D.curNo == 0) {
                                D.item.eq(D.total).hide();
                            } else {
                                D.item.eq(D.curNo - 1).hide();
                            };
                        };
                    });
                } else {
                    if (align != 'ar') {
                        D.item.eq(D.curNo).show().css({ 'left': -(D.itemW) });
                        D.item.animate({ 'left': way + D.itemW }, D.speed, D.type, function () {
                            //if (D.bundle == 1) D.item.eq(D.curNo + 1).hide();
                        });
                    } else {
                        D.item.eq(D.curNo).show().css({ 'left': D.itemW });
                        D.item.animate({ 'left': way + D.itemW }, D.speed, D.type, function () {
                            //if (D.bundle == 1) D.item.eq(D.curNo - 1).hide();
                        });
                    };
                };
            } else if (D.animation == 'fade') {
                D.item.fadeOut(D.speed, D.type).eq(D.curNo).fadeIn(D.speed, D.type);
            };
        },

        pageAction: function (idx, pa) {
            var T = this;
            var way, diff = Math.abs(idx - this.curNo);
            if (T.animation == 'slide') {
                if (idx < this.curNo) {
                    way = '+=';
                    T.item.eq(idx).show().css({ 'left': -(T.itemW) });
                    T.item.animate({ 'left': way + T.itemW }, T.speed, T.type, function () {
                        if (T.bundle == 1) {
                            if (idx != T.total) T.item.eq(idx + 1).hide();
                        };
                    });
                } else {
                    way = '-=';
                    T.item.eq(idx).show().css({ 'left': T.itemW });
                    T.item.animate({ 'left': way + T.itemW }, T.speed, T.type, function () {
                        if (T.bundle == 1) {
                            if (idx != 0) T.item.eq(idx - 1).hide();
                        };
                    });
                };
                T.curNo = idx;
            } else if (this.animation == 'fade') {
                T.item.fadeOut(T.speed, T.type).eq(idx).fadeIn(T.speed, T.type);
                T.curNo = idx;
            };
        },

        update: function () {
            if (this.btn && this.animation != 'fade') {
                if (this.curNo == 0) {
                    this.prev.fadeOut(this.speed);
                    this.next.fadeIn(this.speed);
                } else if (this.curNo == this.total) {
                    this.prev.fadeIn(this.speed);
                    this.next.fadeOut(this.speed);
                } else if (this.curNo > 0 && this.curNo < this.total) {
                    this.prev.fadeIn(this.speed);
                    this.next.fadeIn(this.speed);
                };
            };
            if (this.page) {
                this.pageItem.find('li').removeClass('on').eq(this.curNo).addClass('on');
            };
        },

        settime: function () {
            var T = this;

            clearInterval(T.thisTimer);
            T.thisTimer = setInterval(function () {
                if (T.curNo == T.total) {
                    T.curNo = 0;
                    T.btnAction('ar', true);
                } else if (T.curNo < T.total - T.itemAbat) {
                    T.curNo++;
                    T.btnAction('ar', true);
                };
                T.update();
            }, T.delay);
        },
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 디자인 셀렉트
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $.fn.designSelectElement = function () {
        var selectWrap = '.select_box';
        $('select').each(function (n) {
            $(this).wrap('<div class="select_box"></div>').parents('.select_box').css({ 'width': $(this).outerWidth() }).prepend('<ul></ul>').prepend('<a href="javascript:void(0)"></a>');
            var eTotal = $(this).find('option').length - 1;
            var idx = $(this).find('option:selected').index();

            $(this).find('option').each(function (n) {
                //if (n == 0) $(this).parents(selectWrap).find('> a').attr('href', '#' + $(this).text()).html($(this).text()).addClass('ui_fold_btn');
                $(this).parents('.select_box').find('ul').append('<li><a href="javascript:void(0)">' + $(this).text() + '</a></li>');
            });

            $(this).parents('.select_box').find('> a').attr('href', '#' + $(this).parents('.select_box').find('option').eq(idx).text()).html($(this).find('option').eq(idx).text()).addClass('ui_fold_btn').next('ul').find('li').eq(idx).find('a').addClass('on');

            $(this).hide();
        });
        $('.select_box').find('> a').on('click', function () {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active').siblings('ul').css({ 'width': $(this).parent('.select_box').width() }).slideDown(300);
            } else {
                $(this).removeClass('active').siblings('ul').slideUp(300);
            };
        });
        $('.select_box').find('ul a').on('click', function () {
            var idx = $(this).parent().index();
            var txt = $(this).text();
            var value = $(this).parents('.select_box').find('select').find('option').eq(idx).val();
            $(this).parents('.select_box').find('> a').removeClass('active').text(txt).next('ul').slideUp(300).find('a').removeClass('on').eq(idx).addClass('on');
            $(this).parents('.select_box').find('select').val(value);//.find('option').eq(idx).attr('selected','selected');
            $(this).parents('.select_box').find('select').change();
        });
        $(selectWrap).on('mouseleave', function () {
            $(this).find('> a').removeClass('active').siblings('ul').slideUp(300);
        });
    };
    // $.fn.designSelectElement();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 디자인 체크박스, 라디오
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $.fn.designFormElement = function () {
        $('input[type=checkbox]').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).prop('checked', true).parent('label').addClass('on');
            } else {
                $(this).prop('checked', false).parent('label').removeClass('on');
            };
        });
        $('input[type=radio]').on('click', function () {
            var thisName = $(this).attr('name');
            var thisNo = $('input[name=' + thisName).index(this);
            $('input[name=' + thisName).prop('checked', false).parent('label').removeClass('on')
            $('input[name=' + thisName).eq(thisNo).prop('checked', true).parent('label').addClass('on');
        });
    };
    //$.fn.designFormElement();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 플레이어 셋팅 : 타겟 삽입 - 참고 http://jplayer.org/latest/developer-guide/#jPlayer-option-sizeFull
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $.fn.inMoviePlayer = function (option) {
        var S = $.extend({ target: '.jplayer_wrap', url: '/', width: '100%', height: '100%', link: 'false' }, option);

        this.target = $(S.target);
        this.url = S.url;
        this.width = S.width;
        this.height = S.height;
        this.link = S.link;

        this.pop = S.pop;

        var html = '';
        html += '<div class="jp-video jp-video-360p" id="jp_container_1">';
        html += '   <div class="jp-type-single">';
        html += '       <div id="jquery_jplayer_1" class="jp-jplayer"></div>';
        html += '   </div>';
        html += '</div>';

        this.target.css({ 'position': 'relative' }).append(html).find('#jp_container_1').css({ 'position': 'absolute', 'width': '100%', 'top': 0 });

        this.target.find('.movie_player').css({ 'display': 'none', 'position': 'absolute', 'left': '0', 'top': '0', 'z-index': '999', 'width': this.width, 'height': this.height, 'background-color': '#000' });

        html = '';
        html += '<div class="jp-interface">';
        html += '   <div class="jp-controls">';
        html += '       <button class="jp-play" role="button">play</button>';
        html += '       <button class="jp-pause" role="button">pause</button>';
        html += '       <button class="jp-stop" role="button">stop</button>';
        html += '   </div>';
        html += '   <div class="jp-progress">';
        html += '       <div class="jp-seek-bar">';
        html += '           <div class="jp-play-bar"><a href="#"></a></div>';
        html += '       </div>';
        html += '   </div>';
        html += '   <ul class="timeIng">';
        html += '       <li><span class="jp-current-time" role="timer" aria-label="time">&nbsp;</span></li>';
        html += '       <li><span class="jp-duration" role="timer" aria-label="duration">&nbsp;</span></li>';
        html += '   </ul>	';
        html += '   <div class="jp-volume-controls">';
        html += '       <button class="jp-mute" role="button">mute</button>';
        html += '   </div>';
        html += '   <dl class="movTxt on">';
        html += '       <dt><a href="#"><span>자막보기</span></a></dt>';
        html += '       <dd>';
        html += '           <div class="scroll_mov">';
        html += '           </div>';
        html += '       </dd>';
        html += '   </dl>';
        html += '</div>';

        this.target.find('.jp-type-single').append(html);

        this.player = this.target.find('#jquery_jplayer_1');

        this.init();
    };

    $.fn.inMoviePlayer.prototype = {
        init: function () {
            var T = this;

            T.player.jPlayer({
                ready: function () {
                    $(this).jPlayer('setMedia', {
                        title: '준비',
                        m4v: T.url
                    }).jPlayer('play');
                },
                muted: false,
                swfPath: 'http://www.lottecinemavn.com/LCHS/Script/Common/jplayer.swf',
                solution: 'html, flash',
                supplied: 'webmv, ogv, m4v, mp4',
                preload: 'metadata',
                size: {
                    width: '100%',//T.width,
                    height: '100%'//T.height
                },
                ended: function () {
                    $(this).jPlayer('pause', 0);
                },
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: true,
                remainingDuration: true,
                toggleDuration: true,
                loop: false,
                repeat: false,
                cssSelectorAncestor: '#jp_container_1',
                cssSelector: {
                    play: '.jp-play',
                    pause: '.jp-pause',
                    stop: '.jp-stop',
                    seekBar: '.jp-seek-bar',
                    playBar: '.jp-play-bar',
                    mute: '.jp-mute',
                },
                errorAlerts: false,
                warningAlerts: false
            });

            T.event();
        },

        event: function () {
            var T = this;

            if (T.link != 'false') {
                T.player.on('click', function () {
                    window.open(T.link, '_blank');
                });
            };
        },
    };


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 플레이어 셋팅 : 전체팝업 - 참고 http://jplayer.org/latest/developer-guide/#jPlayer-option-sizeFull
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $.fn.setMoviePlayer = function (option) {


        var S = $.extend({ target: '#wrap', url: '/', width: '100%', height: '100%', play: 'play', stop: 'pause', full: 'full', close: 'colse', link: 'false' }, option);

        this.target = $(S.target);
        this.url = S.url;
        this.width = S.width;
        this.height = S.height;
        this.link = S.link;

        this.play = S.play;
        this.stop = S.stop;
        this.full = S.full;
        this.close = S.close;

        this.target.append('<div class="layer_popup"><div class="jp-video jp-video-360p" id="jp_container_1"><div class="jp-type-single"><div id="jquery_jplayer_1" class="jp-jplayer"></div></div><a href="javascript:void(0);" class="btn_close">Đóng<!--닫기--></a></div></div>');
        this.target.find('.movie_player').css({ 'display': 'none', 'position': 'absolute', 'left': '0', 'top': '0', 'z-index': '999', 'width': this.width, 'height': this.height, 'background-color': '#000' });

        var html = '';
        html += '<div class="jp-interface">';
        html += '   <div class="jp-controls">';
        html += '       <button class="jp-play" role="button">play</button>';
        html += '       <button class="jp-pause" role="button">pause</button>';
        html += '       <button class="jp-stop" role="button">stop</button>';
        html += '   </div>';
        html += '   <div class="jp-progress">';
        html += '       <div class="jp-seek-bar">';
        html += '           <div class="jp-play-bar"></div>';
        html += '       </div>';
        html += '   </div>';
        html += '   <ul class="timeIng">';
        html += '       <li><span class="jp-current-time" role="timer" aria-label="time">&nbsp;</span></li>';
        html += '       <li><span class="jp-duration" role="timer" aria-label="duration">&nbsp;</span></li>';
        html += '   </ul>	';
        html += '   <div class="jp-volume-controls">';
        html += '       <button class="jp-mute" role="button">mute</button>';
        html += '   </div>';
        html += '   <dl class="movTxt on">';
        html += '       <dt><a href="#"><span>Xem phụ đề<!--자막보기--></span></a></dt>';
        html += '       <dd>';
        html += '           <div class="scroll_mov">';
        html += '           </div>';
        html += '       </dd>';
        html += '   </dl>';
        html += '</div>';

        this.target.find('.jp-type-single').append(html);

        this.playerWrap = this.target.find('.layer_popup');
        this.player = this.target.find('#jquery_jplayer_1');

        this.init();

        // 2016.05.10 장착법
        //$(this.playerWrap).find('div').attr('tabindex', '0');
        $(this.playerWrap).attr('tabindex', '0').show().focus();
        //$(this.playerWrap).show().focus();
    };

    $.fn.setMoviePlayer.prototype = {
        init: function () {
            var T = this;

            T.playerWrap.show().find('#jp_container_1').css({ 'position': 'absolute', 'top': '50%', 'left': '50%', 'height': 548, 'margin-left': -485, 'margin-top': -274, 'z-index': '9999' });

            T.player.jPlayer({
                ready: function () {
                    $(this).jPlayer('setMedia', {
                        title: '준비',
                        m4v: T.url
                    }).jPlayer('play');
                },
                muted: false,
                swfPath: 'http://www.lottecinemavn.com/LCHS/Script/Common/jplayer.swf',
                solution: 'html, flash',
                supplied: 'webmv, ogv, m4v, mp4',
                preload: 'metadata',
                size: {
                    width: '100%',//T.width,
                    height: '100%'//T.height
                },
                ended: function () {
                    $(this).jPlayer('pause', 0);
                },
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: false,  // 2016.05.10 장착법
                remainingDuration: true,
                toggleDuration: true,
                loop: false,
                repeat: false,
                cssSelectorAncestor: '#jp_container_1',
                cssSelector: {
                    play: '.jp-play',
                    pause: '.jp-pause',
                    stop: '.jp-stop',
                    seekBar: '.jp-seek-bar',
                    playBar: '.jp-play-bar',
                    mute: '.jp-mute',
                },
                errorAlerts: false,
                warningAlerts: false
            });

            T.event();
        },

        event: function () {
            var R = this;

            R.target.find('.btn_close').on('click', function () {
                R.player.jPlayer('clearMedia');
                R.target.find('.btn_close').off();
                R.playerWrap.remove();

                if (adMovie != "" && adMovie != undefined && adMovie != null) {                    
                    adMovie.focus();
                } else {
                    $('.btn_pause.btn_play').focus();
                }

            });

            if (R.link != 'false') {
                R.player.on('click', function () {
                    window.open(R.link, '_blank');
                });
            };
        },
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 푸터 공지사항 슬라이드
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $.fn.noticeSlideUp = function (option) {
        var S = $.extend({
            methodName: '', target: '', slide: '.event_list', item: 'li', prev: '.btn_prev', next: '.btn_next', play: '.btn_play', stop: '.btn_stop', speed: 500
           , auto: true, delay: 3000, cinemaID: 0
        }, option);

        this.target = $(S.target);
        this.slide = this.target.find(S.slide);
        this.item = S.item;
        this.move = this.slide.height();
        this.curNo = 0;

        this.way = '-='
        this.prev = this.target.find(S.prev);
        this.next = this.target.find(S.next);
        this.play = this.target.find(S.play);
        this.stop = this.target.find(S.stop);
        this.speed = S.speed;
        this.delay = S.delay;
        this.auto = S.auto;
        this.timer;
        this.jsonData;
        this.methodName = S.methodName;
        this.cinemaID = S.cinemaID;

        this.init();
    };

    $.fn.noticeSlideUp.prototype = {
        init: function () {
            var T = this;

            var obj = {};
            var data = null;
            if (T.methodName == 'GetFooterNoticeList')   // 공지사항
            {
                //var obj = { MethodName: "GetMyMovie", channelType: "HO", osType: BrowserAgent(), osVersion: navigator.userAgent, memberOnNo: memberOnNo, filter: filter };
                obj = { MethodName: T.methodName, channelType: "HO", osType: BrowserAgent(), osVersion: navigator.userAgent, multiLanguageID: Language, cinemaAreaCode: 0, cinemaID: T.cinemaID };
                data = JsonReturnDataSync(CinemaServiceDomain + "/LCWS/Customer-Service-Center/CustomerServiceCenterData.aspx", obj);
                if (data.responseJSON.IsOK == "true")
                    T.jsonData = data.responseJSON.Notices.Items;
                else
                    T.jsonData = {};
            }
            else {  // 분실물
                obj = { MethodName: T.methodName, channelType: "HO", osType: BrowserAgent(), osVersion: navigator.userAgent, multiLanguageID: Language, cinemaID: T.cinemaID };
                data = JsonReturnDataSync(CinemaServiceDomain + "/LCWS/Customer-Service-Center/CustomerServiceCenterData.aspx", obj);
                T.jsonData = data.responseJSON.lostArticleItem.Items;
            }

            T.total = T.jsonData.length - 1;

            T.event();
        },

        event: function () {
            var T = this;

            T.slide.html("");

            $(T.jsonData).each(function () {
                var title;
                var code;
                if (T.methodName == 'GetFooterNoticeList') {
                    title = $(this)[0].Title;
                    code = $(this)[0].NoticeID;
                    html = '<li><a href="' + CinemaServerDomain + '/LCHS/Contents/Customer-Service-Center/Notice/notice-detail.aspx?noticeID=' + code + '">' + title + '</a></li>';
                } else {
                    title = $(this)[0].Title;
                    code = $(this)[0].LostArticleID;
                    html = '<li><a href="javascript:customerCenterMenu(4, "2")">' + title + '</a></li>';
                };

                T.slide.append(html);
            });

            T.slide.find(T.item).each(function (n) {
                $(this).css('top', n * T.move);
            });

            T.prev.on('click', function () {
                if (T.slide.find(T.item).is(':animated')) return false;
                if (T.curNo > 0) {
                    T.curNo--;
                } else {
                    T.curNo = T.total;
                };
                T.way = '+=';
                T.animation('prev');
            });
            T.next.on('click', function () {
                if (T.slide.find(T.item).is(':animated')) return false;
                if (T.curNo < T.total) {
                    T.curNo++;
                } else {
                    T.curNo = 0;
                };
                T.way = '-=';
                T.animation('next');
            });

            if (T.auto) {
                T.play.on('click', function () {
                    T.settimer(true);
                });
                T.stop.on('click', function () {
                    T.settimer(false);
                });

                T.settimer(true);
            };
        },

        animation: function (btn) {
            var R = this;

            if (btn == 'prev') {
                R.slide.find(R.item).eq(R.curNo).css({ 'top': -(R.move) });
            } else {
                R.slide.find(R.item).eq(R.curNo).css({ 'top': R.move });
            };

            R.slide.find(R.item).animate({ 'top': R.way + R.move }, R.speed);
        },

        settimer: function (bull) {
            Z = this;
            if (bull) {
                Z.timer = setInterval(function () {
                    Z.next.trigger('click');
                }, Z.delay);
            } else {
                clearInterval(Z.timer);
            };
        },
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 게시판 페이징
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //$.fn.pagingGeneration({ target:타겟 클래스, type:타입코드, total:아이템갯수, view:화면에 보여지는 갯수, searchTarget:검색인풋 클래스, focus:포커스 이동 아이디, page:초기 셋팅 페이지 });
    $.fn.pagingGeneration = function (option) {
        var S = $.extend({ target: '.paging', type: '1', total: 20, view: 12, searchTarget: '', focus: '', page: 1 }, option);

        this.target = $(S.target);
        this.page = S.page;
        this.total = S.total;
        this.view = S.view;
        this.type = S.type;
        this.paging = Math.ceil(this.total / this.view);
        this.group = 1;
        this.groupSet = Math.ceil(this.paging / 10);
        this.pagetWidth = 277;
        this.btns = '';
        this.search = '';
        this.searchTarget = $(S.searchTarget);
        this.focus = $(S.focus);
        this.focusPos;
        if (this.focus != '') this.focusPos = this.focus.offset();

        this.init();
    };

    $.fn.pagingGeneration.prototype = {
        init: function () {
            var T = this;

            T.target.empty();

            T.target.append('<a href="javascript:void(0);" class="prev"><img src="/LCHS/Image/Btn/btn_prev_02.gif" alt="10페이지 이전으로 이동"></a>');
            T.target.append('<span class="pagingNum"></span>');
            if (this.paging > 10) {
                T.target.find('span').css({ 'position': 'relative', 'overflow': 'hidden', 'height': 23, 'width': T.pagetWidth });
            } else {
                T.pagetWidth = (T.paging * 28) - 3
                T.target.find('span').css({ 'position': 'relative', 'overflow': 'hidden', 'height': 23, 'width': T.pagetWidth });
            };

            for (i = 1; i <= T.paging; i++) {
                T.target.find('span').append('<a href="javascript:void(0);">' + i + '</a>').find('a').eq(i - 1).css({ 'position': 'absolute', 'left': ((i - 1) * 28) - 3 });
            };

            T.target.append('<a href="javascript:void(0);" class="next"><img src="/LCHS/Image/Btn/btn_next_02.gif" alt="10페이지 다음으로 이동"></a>');

            if (T.groupSet > 1) {
                T.target.prepend('<a href="javascript:void(0);" class="first"><img src="/LCHS/Image/Btn/btn_first_02.gif" alt="처음으로 이동"></a>');
                T.target.append('<a href="javascript:void(0);" class="last"><img src="/LCHS/Image/Btn/btn_last_02.gif" alt="마지막으로 이동"></a>');
            };

            T.btns = T.target.find('span');

            T.btns.find('a').eq(T.page - 1).addClass('on');

            if (T.page > 10) {
                T.btns.find('a').css({ 'left': '-=' + (Math.floor(T.page / 10) * T.pagetWidth) });
                T.group = Math.ceil(T.page / 10);
            };
            T.event();
        },

        event: function () {
            var R = this;

            R.btns.find('a').on('click', function () {
                (!R.searchTarget) ? R.search = '' : R.search = R.searchTarget.val();
                R.page = $(this).text();
                $(this).addClass('on').siblings('a').removeClass('on');
                //if (R.focusPos != 0) $(window).scrollTop(R.focusPos.top);
                $.fn.gotoPage(R.type, R.search, R.page, R.view);
            });

            this.target.find('> a').on('click', function () {
                (!R.searchTarget) ? R.search = '' : R.search = R.searchTarget.val();

                switch ($(this).attr('class')) {
                    case 'first':
                        R.slideStart();
                        R.update();
                        break;
                    case 'last':
                        R.slideEnd();
                        break;
                    case 'prev':
                        if (R.group > 1) {
                            R.group--;
                            ((parseInt(R.page) - 10) < 1) ? R.page = 1 : R.page = parseInt(R.page) - 10;
                            R.btns.find('a').eq(R.page - 1).addClass('on').siblings('a').removeClass('on');
                            $.fn.gotoPage(R.type, R.search, R.page, R.view);
                            R.slide('+=');
                            R.update();
                        } else {
                            R.slideStart();
                        };
                        break;
                    case 'next':
                        if (R.group < R.groupSet) {
                            R.group++;
                            ((parseInt(R.page) + 10) > R.paging) ? R.page = R.paging : R.page = parseInt(R.page) + 10;
                            R.btns.find('a').eq(R.page - 1).addClass('on').siblings('a').removeClass('on');
                            $.fn.gotoPage(R.type, R.search, R.page, R.view);
                            R.slide('-=');
                            R.update();
                            if (R.group == R.groupSet) {
                                R.btns.css({ 'width': (((R.paging - ((R.group - 1) * 10))) * 28) - 3 });
                            };
                        } else {
                            R.slideEnd();
                        };
                        break;
                };
            });
        },

        slide: function (way) {
            var R = this;

            R.btns = R.target.find('span');

            R.btns.find('a').css({ 'left': way + R.pagetWidth });
        },

        slideStart: function () {
            var R = this;

            R.page = 1;
            R.btns.find('a').eq(R.page - 1).addClass('on').siblings('a').removeClass('on');
            R.btns.find('a').css({ 'left': '+=' + ((R.group - 1) * R.pagetWidth) });
            R.group = 1;
            $.fn.gotoPage(R.type, R.search, 1, R.view);
        },

        slideEnd: function () {
            var R = this;

            R.page = R.paging;
            R.btns.find('a').eq(R.page - 1).addClass('on').siblings('a').removeClass('on');
            R.btns.find('a').css({ 'left': '-=' + ((R.groupSet - R.group) * R.pagetWidth) });
            R.group = R.groupSet;
            R.btns.css({ 'width': (((R.page - ((R.group - 1) * 10))) * 28) - 3 });
            $.fn.gotoPage(R.type, R.search, R.paging, R.view);
        },

        update: function () {
            this.btns.css({ 'width': this.pagetWidth });
        },
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 평점 체크
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $.fn.starRating = function (insFunction) {
        var starPos = $('.score_star .starscore').offset();
        var starWid = $('.score_star .starscore').width();
        var insPos = 0;
        var insStar = 0;
        var fixVar = 5;

        $('.score_star .starscore').on('mousemove', function (e) {
            starPos = $('.score_star .starscore').offset();
            insPos = e.pageX - starPos.left;
            insStar = Math.ceil(insPos / (starWid / 10));
            $(this).find('.starinner').css('width', Math.ceil(insPos));
            $('.score_star .star_sum span').text(insStar);
        });

        $('.score_star .starscore').mouseleave(function () {
            $(this).find('.starinner').css('width', Math.ceil(fixVar * 8));
            $('.score_star .star_sum span').text(fixVar);
        });

        $('.score_star .starscore').click(function () {
            fixVar = insStar;
            insFunction(fixVar);
        });
    };

    // 푸터 일부 링크 새창으로 실행
    var footLink = setTimeout(function () {
        $('.footer_link').find('li').each(function () {
            var footLinkTxt = $(this).find('a').text();
            //console.log(footLinkTxt);
            // 채용안내, 광고/임대문의, 기업정보
            if (footLinkTxt.indexOf("채용안내") > -1 || footLinkTxt.indexOf("광고") > -1 || footLinkTxt.indexOf("기업정보") > -1) {
                $(this).find('a').attr('target', '_blank');
            } else {
                //$(this).find('a').attr('target', '_self');
            }
        });
        // 전체 새창의 경우 주석 풀것
        //$('.footer_link a').attr('target', '_blank');
    }, 1000)
});

var ticketTabPos; // 극장화면 탭 고정 변수

$(window).scroll(function () {
    var scTop = $(window).scrollTop();
    if ($('.c_fixed').hasClass('c_fixed')) {
        var tabTop = $('.c_fixed').offset();

        if (tabTop.top < scTop) {
            $('.bg_fixed').css({ 'position': 'fixed', 'top': 0 }).addClass('on');
        } else {
            $('.bg_fixed').css({ 'position': 'relative' }).removeClass('on');
        };
    };
});

//$(window).load(function () {
//    $('.bannerImg').each(function () {
//        $(this).find('li').each(function () {
//            if (!$(this).find('a').hasClass('btnc_play')) {
//                $(this).css('background', 'url("' + $(this).find('img').attr('src') + '") no-repeat 50% 0').find('img').css('visibility', 'hidden');
//            };
//        });
//    });
//})
