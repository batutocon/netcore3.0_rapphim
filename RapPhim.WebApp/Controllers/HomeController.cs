﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RapPhim.ClientApi.Interfaces;
using RapPhim.Model.Entities;
using RapPhim.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace RapPhim.WebApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        readonly IOrderApiClient _IOrderApiClient;
        
        public HomeController(ILogger<HomeController> logger, IOrderApiClient IOrderApiClient)
        {
            _logger = logger;
            _IOrderApiClient = IOrderApiClient;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> ListPhim(string date)
        {
            DateTime dateParam = !string.IsNullOrEmpty(date) ? DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.CurrentCulture) : DateTime.Now;
            var lstPhim = await _IOrderApiClient.ListPhim(dateParam.Date);
            return PartialView("_PartialListPhim", lstPhim);
        }

        [HttpGet]
        public async Task<ActionResult> LoadLichChieu(int movieid, string date)
        {
            DateTime dateParam = !string.IsNullOrEmpty(date) ? DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.CurrentCulture) : DateTime.Now;
            var lstObj = await _IOrderApiClient.ListLichChieu(movieid, dateParam.Date);
            return PartialView("_PartialSchedule", lstObj);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> LoadSeat(int movieId, int scheduleId)
        {
            var lstObj = await _IOrderApiClient.ListSeat(movieId, scheduleId);
            return PartialView("_PartialSeat", lstObj);
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
